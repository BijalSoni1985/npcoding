﻿namespace NPCoding.Core.Common
{
    using Entities.Domain.Common;
    using System.Collections.Generic;

    /// <summary>
    /// Interface ICommonRepository
    /// </summary>
    public interface ICommonRepository
    {
        /// <summary>
        /// Gets all configuration lookup.
        /// </summary>
        /// <returns>List&lt;ConfigurationLookup&gt;.</returns>
        List<ConfigurationLookup> GetAllConfigurationLookup();

        /// <summary>
        /// Gets all resource object.
        /// </summary>
        /// <returns>List&lt;ResourceObject&gt;.</returns>
        List<ResourceObject> GetAllResourceObject();
    }
}