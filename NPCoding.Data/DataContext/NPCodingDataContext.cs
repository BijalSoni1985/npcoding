﻿namespace NPCoding.Data.DataContext
{
    using Infrastructure;
    using System.Data.Entity;

    /// <summary>
    /// Class NPCodingDataContext.
    /// </summary>
    /// <seealso cref="System.Data.Entity.DbContext" />
    /// <seealso cref="NPCoding.Data.Infrastructure.IDbContext" />
    public class NPCodingDataContext : DbContext, IDbContext
    {
        /// <summary>
        /// Initializes static members of the <see cref="NPCodingDataContext"/> class.
        /// </summary>
        static NPCodingDataContext()
        {
            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
            Database.SetInitializer<NPCodingDataContext>(null);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NPCodingDataContext"/> class.
        /// </summary>
        public NPCodingDataContext() : base("NPCodingDB")
        {
            Database.CommandTimeout = 180;
        }
    }
}
