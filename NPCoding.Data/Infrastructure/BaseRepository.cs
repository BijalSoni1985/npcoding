﻿namespace NPCoding.Data.Infrastructure
{
    using DataContext;

    /// <summary>
    /// Class BaseRepository.
    /// </summary>
    public class BaseRepository
    {
        /// <summary>
        /// The intella quiz data context
        /// </summary>
        private NPCodingDataContext _NPCodingDataContext;

        /// <summary>
        /// Gets the intella quiz data context.
        /// </summary>
        /// <value>The intella quiz data context.</value>
        protected NPCodingDataContext NPCodingDataContext
        {
            get
            {
                if (_NPCodingDataContext == null || string.IsNullOrEmpty(_NPCodingDataContext.Database.Connection.ConnectionString))
                {
                    _NPCodingDataContext = DatabaseFactory.GetNPCodingDataContext();
                }
                return _NPCodingDataContext;
            }
        }

        /// <summary>
        /// Gets the database factory.
        /// </summary>
        /// <value>The database factory.</value>
        protected IDatabaseFactory DatabaseFactory { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseRepository"/> class.
        /// </summary>
        /// <param name="databaseFactory">The database factory.</param>
        protected BaseRepository(IDatabaseFactory databaseFactory)
        {
            DatabaseFactory = databaseFactory;
        }
    }
}
