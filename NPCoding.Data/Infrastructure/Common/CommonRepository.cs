﻿namespace NPCoding.Data.Infrastructure.Common
{
    using Core.Common;
    using Entities.Domain.Common;
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;

    /// <summary>
    /// Class CommonRepository.
    /// </summary>
    /// <seealso cref="NPCoding.Data.Infrastructure.BaseRepository" />
    /// <seealso cref="NPCoding.Core.Common.ICommonRepository" />
    /// <seealso cref="BaseRepository" />
    /// <seealso cref="ICommonRepository" />
    public class CommonRepository : BaseRepository, ICommonRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommonRepository" /> class.
        /// </summary>
        /// <param name="databaseFactory">The database factory.</param>
        public CommonRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

        /// <summary>
        /// Checks the password reset verification code.
        /// </summary>
        /// <param name="verificationCode">The verification code.</param>
        /// <returns>
        /// System.Int64.
        /// </returns>
        public long CheckPasswordResetVerificationCode(string verificationCode)
        {
            try
            {
                return NPCodingDataContext.Database.SqlQuery<long>("EXEC CheckPasswordResetVerificationCode @VerificationCode",
                                new SqlParameter("@VerificationCode", verificationCode)).FirstOrDefault();
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets all configuration lookup.
        /// </summary>
        /// <returns>
        /// List&lt;ConfigurationLookup&gt;.
        /// </returns>
        public List<ConfigurationLookup> GetAllConfigurationLookup()
        {
            try
            {
                return NPCodingDataContext.Database.SqlQuery<ConfigurationLookup>("EXEC GetAllConfigurationLookups").ToList();
            }
            catch (Exception)
            {
                return new List<ConfigurationLookup>();
            }
        }

        /// <summary>
        /// Gets all resource object.
        /// </summary>
        /// <returns>
        /// List&lt;ResourceObject&gt;.
        /// </returns>
        public List<ResourceObject> GetAllResourceObject()
        {
            try
            {
                return NPCodingDataContext.Database.SqlQuery<ResourceObject>("EXEC GetAllResourceObject").ToList();
            }
            catch
            {
                return new List<ResourceObject>();
            }
        }
    }
}