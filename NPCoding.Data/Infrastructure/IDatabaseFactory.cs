﻿namespace NPCoding.Data.Infrastructure
{
    using DataContext;
    using System;

    /// <summary>
    /// Interface IDatabaseFactory
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    public interface IDatabaseFactory : IDisposable
    {
        /// <summary>
        /// Gets the intella quiz data context.
        /// </summary>
        /// <returns>NPCodingDataContext.</returns>
        NPCodingDataContext GetNPCodingDataContext();
    }
}
