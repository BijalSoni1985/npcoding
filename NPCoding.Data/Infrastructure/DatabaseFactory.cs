﻿namespace NPCoding.Data.Infrastructure
{
    using DataContext;

    /// <summary>
    /// Class DatabaseFactory.
    /// </summary>
    /// <seealso cref="NPCoding.Data.Infrastructure.Disposable" />
    /// <seealso cref="NPCoding.Data.Infrastructure.IDatabaseFactory" />
    public class DatabaseFactory : Disposable, IDatabaseFactory
    {
        /// <summary>
        /// The intella quiz data context
        /// </summary>
        private NPCodingDataContext _NPCodingDataContext;

        /// <summary>
        /// Gets the intella quiz data context.
        /// </summary>
        /// <returns>NPCodingDataContext.</returns>
        public NPCodingDataContext GetNPCodingDataContext()
        {
            if (_NPCodingDataContext == null ||string.IsNullOrEmpty(_NPCodingDataContext.Database.Connection.ConnectionString))
            {
                _NPCodingDataContext = new NPCodingDataContext();
            }
            return _NPCodingDataContext;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected override void DisposeCore()
        {
            if (_NPCodingDataContext != null)
            {
                _NPCodingDataContext.Dispose();
            }
        }
    }
}
