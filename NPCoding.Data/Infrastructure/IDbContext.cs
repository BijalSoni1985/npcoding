﻿namespace NPCoding.Data.Infrastructure
{
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;

    /// <summary>
    /// Interface IDbContext
    /// </summary>
    public interface IDbContext
    {
        /// <summary>
        /// Sets this instance.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>DbSet&lt;T&gt;.</returns>
        DbSet<T> Set<T>() where T : class;

        /// <summary>
        /// Entries the specified entity.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity">The entity.</param>
        /// <returns>DbEntityEntry&lt;T&gt;.</returns>
        DbEntityEntry<T> Entry<T>(T entity) where T : class;

        /// <summary>
        /// Gets the database.
        /// </summary>
        /// <value>The database.</value>
        Database Database { get; }

        /// <summary>
        /// Saves the changes.
        /// </summary>
        /// <returns>System.Int32.</returns>
        int SaveChanges();

        /// <summary>
        /// Disposes this instance.
        /// </summary>
        void Dispose();
    }
}
