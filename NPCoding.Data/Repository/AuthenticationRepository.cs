﻿namespace NPCoding.Data.Repository
{
    using Core;
    using Infrastructure;

    /// <summary>
    /// Class AuthenticationRepository.
    /// </summary>
    /// <seealso cref="BaseRepository" />
    /// <seealso cref="IAuthenticationRepository" />
    public class AuthenticationRepository : BaseRepository, IAuthenticationRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AuthenticationRepository"/> class.
        /// </summary>
        /// <param name="databaseFactory">The database factory.</param>
        public AuthenticationRepository(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {
        }
    }
}