﻿namespace NPCoding.Web.Helper
{
    using Common;
    using System.Web;

    /// <summary>
    /// Class SessionManager.
    /// </summary>
    public static class SessionManager
    {
        /// <summary>
        /// Gets or sets the access token.
        /// </summary>
        /// <value>The access token.</value>
        public static string AccessToken
        {
            get
            {
                string value = string.Empty;
                if (HttpContext.Current.Session["AccessToken"] != null)
                {
                    value = HttpContext.Current.Session["AccessToken"].ToSafeString();
                }
                return value;
            }
            set
            {
                HttpContext.Current.Session["AccessToken"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the type of the access.
        /// </summary>
        /// <value>The type of the access.</value>
        public static string AccessType
        {
            get
            {
                string value = string.Empty;
                if (HttpContext.Current.Session["AccessType"] != null)
                {
                    value = HttpContext.Current.Session["AccessType"].ToSafeString();
                }
                return value;
            }
            set
            {
                HttpContext.Current.Session["AccessType"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the culture.
        /// </summary>
        /// <value>The culture.</value>
        public static byte Culture
        {
            get
            {
                byte value = 1;
                if (HttpContext.Current.Session != null && HttpContext.Current.Session["Culture"] != null)
                {
                    value = (byte)HttpContext.Current.Session["Culture"];
                }
                return value;
            }
            set
            {
                HttpContext.Current.Session["Culture"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the expires in.
        /// </summary>
        /// <value>The expires in.</value>
        public static int ExpiresIn
        {
            get
            {
                int value = 60;
                if (HttpContext.Current.Session["ExpiresIn"] != null)
                {
                    value = HttpContext.Current.Session["ExpiresIn"].ToSafeInt();
                }
                return value;
            }
            set
            {
                HttpContext.Current.Session["ExpiresIn"] = value;
            }
        }
    }
}