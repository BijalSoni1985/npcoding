﻿namespace NPCoding.Web.Helper
{
    using Common;
    using Entities.Domain.Common;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Caching;

    public static class CommonMethod
    {
        /// <summary>
        /// Get Resource Value
        /// </summary>
        /// <param name="formName">Form Name</param>
        /// <param name="labelName">Label Name</param>
        /// <param name="culture">Culture Selected</param>
        /// <returns>Value</returns>
        public static string GetResourceValue(string formName, string labelName)
        {
            try
            {
                ResourceObject resourceObject = ResourceObjectList.Where(i => i.FormName == formName && i.LabelName == labelName && i.CultureId == SessionManager.Culture).FirstOrDefault();
                string val = resourceObject == null ? "" : Convert.ToString(resourceObject.DefaultValue);
                val = string.IsNullOrEmpty(val) ? labelName : val;
                return val;
            }
            catch
            {
                return labelName;
            }
        }

        public static async Task<string> GetDetails(string API, string searchBy, int take, int skip, string sortBy, bool sortDir)
        {
            string result = string.Empty;
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("searchBy", searchBy);
            parameters.Add("take", take.ToSafeString());
            parameters.Add("skip", skip.ToSafeString());
            parameters.Add("sortBy", sortBy);
            parameters.Add("sortDir", sortDir.ToSafeString());

            string url = string.Format(API + "?{0}", string.Join("&", parameters.Select(kvp => string.Format("{0}={1}", kvp.Key, kvp.Value))));
            HttpResponseMessage httpResponseMessage = await NPCodingApplication.httpClientInstance.PostAsync(url, null);
            if (httpResponseMessage.IsSuccessStatusCode)
            {
                ///result = httpResponseMessage.Content.ReadAsAsync<string>().Result;
                result = httpResponseMessage.Content.ReadAsStringAsync().Result.ToString();
            }
            return result;
        }

        /// <summary>
        /// Gets the resource object list.
        /// </summary>
        /// <value>The resource object list.</value>
        private static List<ResourceObject> ResourceObjectList
        {
            get
            {
                if (HttpRuntime.Cache.Get(Constants.RESOURCEOBJECTLIST) != null)
                {
                    return (List<ResourceObject>)HttpRuntime.Cache.Get(Constants.RESOURCEOBJECTLIST);
                }
                else
                {
                    HttpResponseMessage httpResponseMessage = NPCodingApplication.httpClientInstance.GetAsync(API.GetResources).Result;
                    List<ResourceObject> resourceObjectList = JsonConvert.DeserializeObject<List<ResourceObject>>(httpResponseMessage.Content.ReadAsStringAsync().Result);
                    HttpRuntime.Cache.Remove(Constants.RESOURCEOBJECTLIST);
                    HttpRuntime.Cache.Insert(Constants.RESOURCEOBJECTLIST, resourceObjectList, null, DateTime.Today.AddYears(1), Cache.NoSlidingExpiration);
                    return (List<ResourceObject>)HttpRuntime.Cache.Get(Constants.RESOURCEOBJECTLIST);
                }
            }
        }

        private static List<ConfigurationLookup> ConfigurationLookupList
        {
            get
            {
                if (HttpRuntime.Cache.Get(Constants.CONFIGURATIONLOOKUP) != null)
                {
                    return (List<ConfigurationLookup>)HttpRuntime.Cache.Get(Constants.CONFIGURATIONLOOKUP);
                }
                else
                {
                    HttpResponseMessage httpResponseMessage = NPCodingApplication.httpClientInstance.GetAsync(API.GetConfigurationLookups).Result;
                    List<ConfigurationLookup> configurationLookupList = JsonConvert.DeserializeObject<List<ConfigurationLookup>>(httpResponseMessage.Content.ReadAsStringAsync().Result);
                    HttpRuntime.Cache.Remove(Constants.CONFIGURATIONLOOKUP);
                    HttpRuntime.Cache.Insert(Constants.CONFIGURATIONLOOKUP, configurationLookupList, null, DateTime.Today.AddYears(1), Cache.NoSlidingExpiration);
                    return (List<ConfigurationLookup>)HttpRuntime.Cache.Get(Constants.CONFIGURATIONLOOKUP);
                }
            }
        }

        /// <summary>
        /// Gets the configuration lookup.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>System.String.</returns>
        public static string GetConfigurationLookup(string key)
        {
            try
            {
                return ConfigurationLookupList.Where(x => x.KeyField == key).Select(y => y.ValueField).FirstOrDefault() ?? string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}