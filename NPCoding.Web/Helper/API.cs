﻿namespace NPCoding.Web.Helper
{
    /// <summary>
    /// Class API.
    /// </summary>
    public static class API
    {
        /// <summary>
        /// The token
        /// </summary>
        public const string Token = "Token";

        /// <summary>
        /// The log out
        /// </summary>
        public const string LogOut = "API/Common/LogOut";

        /// <summary>
        /// The get configuration lookups
        /// </summary>
        public const string GetConfigurationLookups = "API/Common/GetAllConfigurations";

        /// <summary>
        /// The get resources
        /// </summary>
        public const string GetResources = "API/Common/GetResources";
    }
}