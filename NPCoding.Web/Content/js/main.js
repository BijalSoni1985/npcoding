$(function () {
    $("#wizard").steps({
        headerTag: "h4",
        bodyTag: "section",
        transitionEffect: "fade",
        enableAllSteps: true,
        transitionEffectSpeed: 300,
        labels: {
            next: continueText,
            previous: backText,
            finish: getEstimationText
        },
        onStepChanging: function (event, currentIndex, newIndex) {
            if (newIndex == 0) {
                $('.actions ul li').eq(1).find('a').hide();
                $('.actions ul li:first').find('a').hide();
                switch ($('.platform.active').attr('href').replace('#', '').toLowerCase()) {
                    case web:
                        $('.actions ul li').eq(1).find('a').hide();
                        break;
                    case mobile:
                        $('.actions ul li').eq(1).find('a').hide();
                        break;
                    case webMobile:
                        $('.actions ul li').eq(1).find('a').hide();
                        break;
                    case mobilepoc:
                        $('.actions ul li').eq(1).find('a').show();
                        break;
                    case crawling:
                        $('.actions ul li').eq(1).find('a').show();
                        break;
                    default:
                        $('.actions ul li').eq(1).find('a').hide();
                        break;
                }
            }
            else {
                $('.actions ul li:first').find('a').show();
                $('.actions ul li').eq(1).find('a').show();
            }

            if (newIndex >= 1) {
                $('.steps ul li:first-child a img');
            } else {
                $('.steps ul li:first-child a img');
            }

            if (newIndex === 1) {
                $('.actions ul li').eq(1).find('a').hide();
                $('.steps ul li:nth-child(2) a img');
            } else {
                $('.steps ul li:nth-child(2) a img');
            }


            if (newIndex === 2) {
                $('.last').next('li').find('a').click();
                $('.steps ul li:nth-child(3) a img');
                $('.actions ul li:first').find('a').show();
                $('.actions ul li').eq(1).find('a').hide();
                $('.steps ul li:nth-child(4) a img');
                $('.actions ul').addClass('step-4');
            } else {
                $('.steps ul li:nth-child(3) a img');
            }

            if (newIndex === 3) {
                $('html, body').animate({
                    scrollTop: $("div#stepsSection").offset().top + 100
                }, 900);
                $('li.current').nextAll('li').slice(0, 2).show();
                $('li.current').find('.step-arrow').show();
                $('li.current').next('li').find('.step-arrow').show();
                $('.steps ul li:nth-child(4) a img');
                $('.actions ul').addClass('step-4');
            } else {
                $('.steps ul li:nth-child(4) a img');
                $('.actions ul').removeClass('step-4');
            }

            if (newIndex === 4) {
                $('html, body').animate({
                    scrollTop: $("div#stepsSection").offset().top - 100
                }, 900);
                //$('li.current').nextAll('li').slice(0, 2).show();
                $('.actions ul li:first').find('a').show();
                $('.actions ul li').eq(1).find('a').show();
                $('.steps ul li:nth-child(4) a img');
                $('.actions ul').addClass('step-4');
            } else {
                $('.steps ul li:nth-child(4) a img');
                $('.actions ul').removeClass('step-4');
            }
            return true;
        },
        onFinished: function (event, currentIndex) {
            GetEstimation();
        }
    });
    // Custom Button Jquery Steps
    $('.forward').click(function () {
        $("#wizard").steps('next');
    })
    $('.backward').click(function () {
        $("#wizard").steps('previous');
    })
    // Click to see password
    $('.password i').click(function () {
        if ($('.password input').attr('type') === 'password') {
            $(this).next().attr('type', 'text');
        } else {
            $('.password input').attr('type', 'password');
        }
    })
    // Create Steps Image
    $('.steps ul li:first-child').find('a').append('<img src="../Content/images/step-arrow.png" alt="" class="step-arrow">').append('<span class="step-order">' + categories + '</span>');
    $('.steps ul li:nth-child(2)').find('a').append('<img src="../Content/images/step-arrow.png" alt="" class="step-arrow">').append('<span class="step-order">' + platformes + '</span>');
    $('.steps ul li:nth-child(3)').find('a').append('<img src="../Content/images/step-arrow.png" alt="" class="step-arrow">').append('<span class="step-order">' + technologies + '</span>');
    $('.steps ul li:nth-child(4)').find('a').append('<img src="../Content/images/step-arrow.png" alt="" class="step-arrow">').append('<span class="step-order">' + functionalities + '</span>');
    $('.steps ul li:nth-child(5)').find('a').append('<span class="step-order">Final Step</span>');
    // Count input
    $(".quantity span").on("click", function () {
        var $button = $(this);
        var oldValue = $button.parent().find("input").val();
        var newVal;

        if ($button.hasClass('plus')) {
            newVal = parseFloat(oldValue) + 1;
        } else {
            // Don't allow decrementing below zero
            if (oldValue > 0) {
                newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0;
            }
        }
        $button.parent().find("input").val(newVal);
    });
});