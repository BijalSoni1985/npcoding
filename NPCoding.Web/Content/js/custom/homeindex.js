﻿$(document).ready(function () {

    $('#step').click(function () {
        $('html, body').animate({
            scrollTop: $("div#stepsSection").offset().top
        }, 1000);
    });
    $(document).on('click', '.platform', function () {
        switch ($(this).attr('href').replace('#', '').toLowerCase()) {
            case web:
                $('#SelectedPlatform').val(webVal);
                $('.webtechnologydiv,.webfunctionalitydiv').removeAttr('style');
                $('.webfunctionalitydiv').addClass('in active show');
                $('.mobiletechnologydiv,.mobilefunctionalityli,.mobilefunctionalitydiv').attr('style', 'display:none;').removeClass('in active show');
                $('.POC-text').hide();
                $('.CrawlingPOC-text').hide();
                $('li.current').find('.step-arrow').show();
                $('li.current').next('li').find('.step-arrow').hide();
                $('li.current').nextAll('li').slice(0, 1).show();
                $('li.current').nextAll('li').slice(1, 3).hide();
                $('.current').next('li').find('a').click();
                $('.actions ul li').eq(1).find('a').unbind('click').on('click', function () {
                    $('li.current').next('li').find('a').click();
                });
                $('li.last').hide();
                break;
            case mobile:
                $('#SelectedPlatform').val(mobileVal);
                $('.mobiletechnologydiv,.mobilefunctionalityli,.mobilefunctionalitydiv').removeAttr('style');
                $('.mobilefunctionalityli').addClass('in active show');
                $('.mobilefunctionalitydiv').addClass('in active show');
                $('.webtechnologydiv,.webfunctionalitydiv').attr('style', 'display:none;').removeClass('in active show');
                $('.POC-text').hide();
                $('.CrawlingPOC-text').hide();
                $('li.current').find('.step-arrow').show();
                $('li.current').nextAll('li').slice(0, 1).show();
                $('li.current').nextAll('li').slice(1, 3).hide();
                $('.current').next('li').find('a').click();
                $('.actions ul li').eq(1).find('a').unbind('click').on('click', function () {
                    $('li.current').next('li').find('a').click();
                });
                if ($('li.last').is(':visible')) {
                    $('li.current').find('.step-arrow').show();
                }
                else {
                    $('li.current').find('.step-arrow').hide();
                }
                $('li.last').hide();
                break;
            case webMobile:
                $('#SelectedPlatform').val(webMobileVal);
                $('.webtechnologydiv,.webfunctionalitydiv').removeAttr('style');
                $('.webfunctionalitydiv').addClass('in active show');
                $('.mobiletechnologydiv,.mobilefunctionalityli,.mobilefunctionalitydiv').removeAttr('style').removeClass('in active show');
                $('.POC-text').hide();
                $('.CrawlingPOC-text').hide();
                $('li.current').find('.step-arrow').show();
                $('li.current').nextAll('li').slice(0, 1).show();
                $('.current').next('li').find('a').click();
                $('.actions ul li').eq(1).find('a').unbind('click').on('click', function () {
                    $('li.current').next('li').find('a').click();
                });
                $('li.last').hide();
                break;
            case mobilepoc:
                $('#SelectedPlatform').val(mobilepocVal);
                $('.mobiletechnologydiv,.mobilefunctionalityli,.mobilefunctionalitydiv').removeAttr('style');
                $('.mobilefunctionalityli').addClass('in active show');
                $('.mobilefunctionalitydiv').addClass('in active show');
                $('.webtechnologydiv,.webfunctionalitydiv').attr('style', 'display:none;').removeClass('in active show');
                $('.POC-text').removeAttr('style');
                $('.POC-text').css({ "padding-top": "15px" });
                $('.CrawlingPOC-text').hide();
                $('.actions ul li').eq(1).find('a').show().unbind('click').on('click', function () {
                    $('li.last').prevAll('li:visible').eq(0).find('.step-arrow').show();
                    $('li.last').show();
                    $('li.last').find('a').click();
                });
                $('#web').removeAttr('style').removeClass('in active show');
                $('li.current').nextAll('li').slice(0, 2).hide();
                if ($('li.last').is(':visible')) {
                    $('li.current').find('.step-arrow').show();
                }
                else {
                    $('li.current').find('.step-arrow').hide();
                }
                break;
            case crawling:
                $('#SelectedPlatform').val(crawlingVal);
                $('.webtechnologydiv,.webfunctionalitydiv').removeAttr('style');
                $('.webfunctionalitydiv').addClass('in active show');
                $('.mobiletechnologydiv,.mobilefunctionalityli,.mobilefunctionalitydiv').removeAttr('style').removeClass('in active show');
                $('.POC-text').hide();
                $('.CrawlingPOC-text').removeAttr('style');
                $('#web').removeAttr('style').removeClass('in active show');
                $('.actions ul li').eq(1).find('a').show().unbind('click').on('click', function () {
                    $('li.last').prevAll('li:visible').eq(0).find('.step-arrow').show();
                    $('li.last').show();
                    $('li.last').find('a').click();
                });
                $('.CrawlingPOC-text').css({ "padding-top": "15px" });
                $('li.current').nextAll('li').slice(0, 2).hide();
                $('li.current').find('.step-arrow').hide();
                if ($('li.last').is(':visible')) {
                    $('li.current').find('.step-arrow').show();
                }
                else {
                    $('li.current').find('.step-arrow').hide();
                }
                break;
            default:
                $('#SelectedPlatform').val(webVal);
                $('.webtechnologydiv,.webfunctionalitydiv').removeAttr('style');
                $('.webfunctionalitydiv').addClass('in active show');
                $('.mobiletechnologydiv,.mobilefunctionalityli,.mobilefunctionalitydiv').attr('style', 'display:none;').removeClass('in active show');
                $('li.current').nextAll('li').slice(0, 1).show();
                $('.first').next('li').find('a').click();
                $('.actions ul li').eq(1).find('a').unbind('click').on('click', function () {
                    $('li.current').next('li').find('a').click();
                });
                $('li.last').hide();
                break;
        }
    });

    $(document).on('click', '.categoryplatform', function () {
        switch ($(this).attr('href').replace('#', '').toLowerCase()) {
            case travel:
                $('#ProjectCategory').val(travelVal);
                $('.first').next('li').find('a').click();
                break;
            case digital:
                $('#ProjectCategory').val(digitalVal);
                $('.first').next('li').find('a').click();
                break;
            case dataSecurity:
                $('#ProjectCategory').val(dataSecurityVal);
                $('.first').next('li').find('a').click();
                break;
            case productivity:
                $('#ProjectCategory').val(productivityVal);
                $('.first').next('li').find('a').click();
                break;
            case gaming:
                $('#ProjectCategory').val(gamingVal);
                $('.first').next('li').find('a').click();
                break;
            case business:
                $('#ProjectCategory').val(businessVal);
                $('.first').next('li').find('a').click();
                break;
            case educational:
                $('#ProjectCategory').val(educationalVal);
                $('.first').next('li').find('a').click();
                break;
            case lifestyle:
                $('#ProjectCategory').val(lifestyleVal);
                $('.first').next('li').find('a').click();
                break;
            case entertainment:
                $('#ProjectCategory').val(entertainmentVal);
                $('.first').next('li').find('a').click();
                break;
            case utility:
                $('#ProjectCategory').val(utilityVal);
                $('.first').next('li').find('a').click();
                break;
            case other:
                $('#ProjectCategory').val(otherVal);
                $('.first').next('li').find('a').click();
                break;
            default:
                $('#ProjectCategory').val(otherVal);
                $('.first').next('li').find('a').click();
                break;
        }
    });

    $(document).on('click', '.webtechnology', function () {
        switch ($(this).attr('href').replace('#', '').toLowerCase()) {
            case react:
                $('#SelectedWebTechnology').val(reactVal);
                $('.webtechnologyamountdiv').show();
                break;
            case angular:
                $('#SelectedWebTechnology').val(angularVal);
                $('.webtechnologyamountdiv').show();
                break;
            case jquery:
                $('#SelectedWebTechnology').val(jqueryVal);
                $('.webtechnologyamountdiv').show();
                break;
            case eCommerce:
                $('#SelectedWebTechnology').val(ecommerceVal);
                $('.webtechnologyamountdiv').show();
                break;
            default:
                $('#SelectedWebTechnology').val(reactVal);
                $('.webtechnologyamountdiv').show();
                $('.actions ul li').eq(1).find('a').show();
                break;
        }


        $('.numeric').keyup = null;

        $('.numeric').keyup(function (e) {
            manageContinueButton();
        });
    });

    $(document).on('click', '.mobiletechnology', function () {
        switch ($(this).attr('href').replace('#', '').toLowerCase()) {
            case android:
                $('#SelectedMobileTechnology').val(androidVal);
                $('.mobiletechnologyamountdiv').show();
                break;
            case iphone:
                $('#SelectedMobileTechnology').val(iphoneVal);
                $('.mobiletechnologyamountdiv').show();
                break;
            case both:
                $('#SelectedMobileTechnology').val(bothVal);
                $('.mobiletechnologyamountdiv').show();
                break;
            default:
                $('#SelectedMobileTechnology').val(androidVal);
                $('.mobiletechnologyamountdiv').show();
                $('.actions ul li').eq(1).find('a').show();
                break;
        }

        $('.numeric').keyup = null;

        $('.numeric').keyup(function (e) {
            manageContinueButton();
        });
    });

    $(document).on('click', '#EstimateOtherProject', function () {
        resetForm();
    });

    $(document).on('click', '.category-row > .custom-radio > input:radio', function () {
        $('li.current').next('li').find('a').click();
    });

    $(document).on('keypress', '.numeric', function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
    $(document).on('click', '#DesignsWeb', function () {
        $('html, body').animate({
            scrollTop: $("div#stepsSection").offset().top + 300
        }, 900);
    });
    $(document).on('click', '#DesignsMobile', function () {
        $('html, body').animate({
            scrollTop: $("div#stepsSection").offset().top + 400
        }, 900);
    });
    $(document).on('click', '.steps > ul > li.current > a', function () {
        if ($(this).attr('href').replace('#', '') == 'wizard-h-1') {
            switch ($('#SelectedPlatform').val()) {
                case mobilepocVal:
                    $('.actions ul li').eq(1).find('a').show();
                    break;
                case crawlingVal:
                    $('.actions ul li').eq(1).find('a').show();
                    break;
                default:
                    $('.actions ul li').eq(1).find('a').hide();
                    break;
            }
        }
        else if ($(this).attr('href').replace('#', '') == 'wizard-h-2') {
            switch ($('#SelectedPlatform').val()) {
                case webVal:
                    if ($("#AmountOfScreenWeb").val() != "") {
                        $('.actions ul li').eq(1).find('a').show();
                    }
                    else {
                        $('.actions ul li').eq(1).find('a').hide();
                    }
                    break;
                case mobileVal:
                    if ($("#AmountOfScreenMobile").val() != "") {
                        $('.actions ul li').eq(1).find('a').show();
                    }
                    else {
                        $('.actions ul li').eq(1).find('a').hide();
                    }
                    break;
                case webMobileVal:
                    if ($("#AmountOfScreenWeb").val() != "" && $("#AmountOfScreenMobile").val() != "") {
                        $('.actions ul li').eq(1).find('a').show();
                    }
                    else {
                        $('.actions ul li').eq(1).find('a').hide();
                    }
                    break;
                default:
                    $('.actions ul li').eq(1).find('a').show();
                    break;
            }
        }
    });
    setTimeout(function () {
        $('.actions ul li:first').find('a').hide();
        $('.actions ul li').eq(1).find('a').hide();
        $('li.current').nextAll('li').slice(1, 4).hide();
        $('li.current').nextAll('li').find('.step-arrow').hide();
    }, 100);
});

function resetForm() {
    $('.loader-box').hide();
    $('.loader-div').show();
    $('#estimationDiv').hide();
    $('#captchaError').hide();
    document.getElementById("wizard").reset();
    $('.first a').click();
    $('.first').addClass('current').siblings().removeClass('done current');
    $('.categoryplatform').removeClass('active show');
    $('.platform').removeClass('active show');
    $('.webtechnology').removeClass('active show');
    $('.mobiletechnology').removeClass('active show');
    $('.mobilefunctionalityli').removeClass('active show');
    $('.mobiletechnologydiv').attr('style', 'display:none;');
    $('.mobilefunctionalityli').attr('style', 'display:none;');
    $('.mobilefunctionalityli a').removeClass('active show');
    $('.POC-text').hide();
    $('.CrawlingPOC-text').hide();
    $('.actions ul li').eq(1).find('a').hide();
    $('#wizard').show();
    $('li.current').nextAll('li').slice(1, 4).hide();
    $('li.current').nextAll('li').find('.step-arrow').hide();
}

function ValidateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function GetEstimation() {
    $('#ClientEmail').removeAttr('style');
    if (!$('#ClientEmail').val()) {
        $('#ClientEmail').attr('style', 'border:1px solid red;').focus();
        return false;
    }

    if (ValidateEmail($('#ClientEmail').val())) {
        $('#wizard').hide();
        $('.loader-box').removeAttr("style");
        $('.text-slider').show();

        $('.text-slider').slick({
            dots: false,
            infinite: true,
            speed: 2000,
            fade: true,
            cssEase: 'linear',
            arrows: false,
            autoplay: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplaySpeed: 2000,
        });

        grecaptcha.ready(function () {
            grecaptcha.execute(googleCaptchaKey, { action: 'homepage' }).then(function (token) {
                fetch(verifyCaptcha, {
                    method: "POST", headers: { "Content-Type": "application/json; charset=utf-8" }, body: JSON.stringify({
                        token: token
                    })
                })
                    .then(res => res.json())
                    .then(response => {
                        if (response.Data.score > 0.5) {
                            fetch(getEstimationUrl, {
                                method: "POST", headers: { "Content-Type": 'application/x-www-form-urlencoded; charset=UTF-8' }, body: $("#wizard").serialize()
                            })
                                .then(res => res.json())
                                .then(response => {
                                    setTimeout(function () {
                                        if (response.IsSuccess) {
                                            if (response.IsSent) {
                                                $('.loader-div').hide();
                                                $('#estimationDiv').show();
                                                $('.text-slider').hide();
                                            }
                                            else {
                                                alert("There was some issue while processing your request, please try again later.");
                                            }
                                        }
                                        else {
                                            $('#ClientEmail').attr('style', 'border:1px solid red;').focus();
                                        }}, 9000);
                                })
                                .catch(err => {
                                    alert("Error getting estimation. Please try again later.");
                                    resetForm();
                                });
                        }
                        else {
                            $('.loader-div').hide();
                            $('#captchaError').show();
                            $('.text-slider').hide();
                        }
                    })
                    .catch(err => {
                        alert("Error getting estimation. Please try again later.");
                        resetForm();
                    });
            });
        });
    }
    else {
        $('#ClientEmail').attr('style', 'border:1px solid red;').focus();
    }
}

function manageContinueButton() {
    switch ($('#SelectedPlatform').val()) {
        case webVal:
            if ($('#AmountOfScreenWeb').val() != "") {
                $('html, body').animate({
                    scrollTop: $("div#stepsSection").offset().top + 150
                }, 900);
                showContinueButton();
            }
            else {
                hideContinueButton();
            }
            break;
        case mobileVal:
            if ($('#AmountOfScreenMobile').val() != "") {
                $('html, body').animate({
                    scrollTop: $("div#stepsSection").offset().top + 150
                }, 900);
                showContinueButton();
            }
            else {
                hideContinueButton();
            }
            break;
        case webMobileVal:
            if ($('#AmountOfScreenWeb').val() != "" && $('#AmountOfScreenMobile').val() != "") {
                $('html, body').animate({
                    scrollTop: $("div#stepsSection").offset().top + 650
                }, 900);
                showContinueButton();
            }
            else {
                $('html, body').animate({
                    scrollTop: $("div#stepsSection").offset().top + 450
                }, 900);
                hideContinueButton();
            }
            break;
        default:
            hideContinueButton();
            break;
    }
}

function showContinueButton() {
    $('.actions ul li').eq(1).find('a').show();
    $('li.current').nextAll('li').slice(0, 4).show();
    $('li.current').find('.step-arrow').show();
    $('li.current').nextAll('li').find('.step-arrow').show();
}

function hideContinueButton() {
    $('.actions ul li').eq(1).find('a').hide();
    $('li.current').nextAll('li').slice(0, 4).hide();
    $('li.current').find('.step-arrow').hide();
}