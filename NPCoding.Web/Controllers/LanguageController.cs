﻿using System;
using System.Web;
using System.Web.Mvc;
using static NPCoding.Web.Helper.ManageLanguage;

namespace NPCoding.Web.Controllers
{
    public class LanguageController : Controller
    {
        public LanguageController()
        {
            new LanguageMang().SetLanguage("en");
        }

        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            string lang = null;
            HttpCookie langCookie = Request.Cookies["culture"];
            if (langCookie != null)
            {
                lang = langCookie.Value;
            }
            else
            {
                var userLanguage = Request.UserLanguages;
                var userLang = userLanguage != null ? userLanguage[0] : "";
                if (userLang != "")
                {
                    lang = userLang;
                }
                else
                {
                    lang = LanguageMang.GetDefaultLanguage();
                }
            }
            new LanguageMang().SetLanguage(lang);
            return base.BeginExecuteCore(callback, state);
        }
    }
}