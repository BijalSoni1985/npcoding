﻿using NPCoding.Common;
using NPCoding.Email;
using NPCoding.Entities.Request;
using NPCoding.Entities.Response;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;
using static NPCoding.Web.Helper.ManageLanguage;

namespace NPCoding.Web.Controllers
{
    //public class HomeController : LanguageController
    public class HomeController : Controller
    {
        /// <summary>
        /// The API address
        /// </summary>
        private readonly string apiAddress = Utils.ConfigKey("GoogleCaptchaVerifyUrl");

        /// <summary>
        /// The recaptcha secret
        /// </summary>
        private readonly string recaptchaSecret = Utils.ConfigKey("GoogleCaptchaSecretKey");

        private readonly string currency = Utils.ConfigKey("Currency");

        /// <summary>
        /// The web fixed amount
        /// </summary>
        private readonly double webFixedAmount = 5000;

        /// <summary>
        /// The web per screen fixed amount
        /// </summary>
        private readonly double webPerScreenFixedAmount = 2000;

        /// <summary>
        /// The web per design fixed amount
        /// </summary>
        private readonly double webPerDesignFixedAmount = 800;

        /// <summary>
        /// The web database fixed amount
        /// </summary>
        private readonly double webDatabaseFixedAmount = 20000;

        /// <summary>
        /// The web API fixed amount
        /// </summary>
        private readonly double webAPIFixedAmount = 20000;

        /// <summary>
        /// The web security layer fixed amount
        /// </summary>
        private readonly double webSecurityLayerFixedAmount = 1000;

        /// <summary>
        /// The web cloud server fixed amount
        /// </summary>
        private readonly double webCloudServerFixedAmount = 1500;

        /// <summary>
        /// The web external API fixed amount
        /// </summary>
        private readonly double webExternalApiFixedAmount = 4000;

        /// <summary>
        /// The web special SDK fixed amount
        /// </summary>
        private readonly double webSpecialSdkFixedAmount = 4000;

        /// <summary>
        /// The mobile fixed amount
        /// </summary>
        private readonly double mobileFixedAmount = 15000;

        /// <summary>
        /// The mobile per screen fixed amount
        /// </summary>
        private readonly double mobilePerScreenFixedAmount = 2000;

        /// <summary>
        /// The mobile per design fixed amount
        /// </summary>
        private readonly double mobilePerDesignFixedAmount = 800;

        /// <summary>
        /// The mobile database fixed amount
        /// </summary>
        private readonly double mobileDatabaseFixedAmount = 20000;

        /// <summary>
        /// The mobile API fixed amount
        /// </summary>
        private readonly double mobileAPIFixedAmount = 20000;

        /// <summary>
        /// The mobile security layer fixed amount
        /// </summary>
        private readonly double mobileSecurityLayerFixedAmount = 1000;

        /// <summary>
        /// The mobile cloud server fixed amount/
        /// </summary>
        private readonly double mobileCloudServerFixedAmount = 1500;

        /// <summary>
        /// The mobile external API fixed amount
        /// </summary>
        private readonly double mobileExternalApiFixedAmount = 4000;

        /// <summary>
        /// The mobile special SDK fixed amount
        /// </summary>
        private readonly double mobileSpecialSdkFixedAmount = 4000;

        /// <summary>
        /// The urgency fixed amount
        /// </summary>
        private readonly double urgencyFixedAmount = 1.5;

        private readonly string currentCulture = "en";

        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Verifies the captcha.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult VerifyCaptcha(string token)
        {
            var responseString = Task.Run(async () => await RecaptchaVerify(token));
            ResponseToken response = new ResponseToken();
            response = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseToken>(responseString.Result);

            return Json(new { Data = response }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the estimation.
        /// </summary>
        /// <param name="estimation">The estimation.</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetEstimation(Estimation estimation)
        {
            double estimatedAmount = 0;
           // EmailSMTP emailSmtp = new EmailSMTP();
            EmailSendGrid emailSendGrid = new EmailSendGrid();
            EmailSMTP emailSMTP = new EmailSMTP();
            EmailRequest emailRequest = new EmailRequest();
            bool isSuccess = false;

            if (Utils.IsValidEmail(estimation.ClientEmail))
            {
                estimatedAmount = CalculateEstimation(estimation);
                estimatedAmount = estimation.Urgency == "on" ? (estimatedAmount * urgencyFixedAmount) : estimatedAmount;
                estimatedAmount = estimation.Developer == "Outsource" ? (0.65 * estimatedAmount) : estimatedAmount;

                isSuccess = true;

                string htmlFileName = DateTime.Now.Ticks.ToString();

                estimation.ClientMobile = estimation.ClientMobile.ToSafeString() != string.Empty ? estimation.ClientMobile : "-";

                emailRequest = SetClientEmailParams(estimation, emailRequest, estimatedAmount, htmlFileName);
                Tuple<string, bool> emailResponse = null;

                if (Utils.ConfigKey("SendEmailVia") == "sendgrid")
                {
                    emailResponse = Task.Run(async () => await emailSendGrid.SendEmail(emailRequest)).Result;
                }
                else
                {
                    emailResponse = Task.Run(async () => await emailSMTP.SendEmail(emailRequest)).Result;
                }

                if (emailResponse != null && emailResponse.Item2)
                {
                    emailRequest = new EmailRequest();

                    emailRequest = SetAdminEmailParams(estimation, emailRequest, estimatedAmount, htmlFileName);

                    Tuple<string, bool> emailAdminResponse = null;
                    if (Utils.ConfigKey("SendEmailVia") == "sendgrid")
                    {
                        emailAdminResponse = Task.Run(async () => await emailSendGrid.SendEmail(emailRequest)).Result;
                    }
                    else
                    {
                        emailAdminResponse = Task.Run(async () => await emailSMTP.SendEmail(emailRequest)).Result;
                    }

                    GenerateLog(estimation.ClientEmail, estimation.ClientMobile, estimatedAmount, htmlFileName);
                }

                return Json(new { IsSuccess = isSuccess, Message = emailResponse.Item1, IsSent = emailResponse.Item2, EstimatedAmount = estimatedAmount }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { IsSuccess = isSuccess }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Changes the language.
        /// </summary>
        /// <param name="lang">The language.</param>
        /// <returns></returns>
        public ActionResult ChangeLanguage(string lang)
        {
            new LanguageMang().SetLanguage(lang);
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Recaptchas the verify.
        /// </summary>
        /// <param name="recaptchaToken">The recaptcha token.</param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        private async Task<string> RecaptchaVerify(string recaptchaToken)
        {
            string url = apiAddress + "?secret=" + recaptchaSecret + "&response=" + recaptchaToken;
            using (var httpClient = new HttpClient())
            {
                try
                {
                    string responseString = httpClient.GetStringAsync(url).Result;
                    return responseString;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        /// <summary>
        /// Calculates the estimation.
        /// </summary>
        /// <param name="estimation">The estimation.</param>
        /// <returns></returns>
        private double CalculateEstimation(Estimation estimation)
        {
            switch (estimation.SelectedPlatform)
            {
                case (int)EnumData.Platform.Web:
                    return CalculateWebEstimation(estimation);

                case (int)EnumData.Platform.Mobile:
                    return CalculateMobileEstimation(estimation);

                case (int)EnumData.Platform.WebMobile:
                    return CalculateWebEstimation(estimation) + CalculateMobileEstimation(estimation);
                default:
                    return CalculateWebEstimation(estimation);
            }
        }

        /// <summary>
        /// Calculates the web estimation.
        /// </summary>
        /// <param name="estimation">The estimation.</param>
        /// <returns></returns>
        private double CalculateWebEstimation(Estimation estimation)
        {
            double webEstimatedAmount = 0;

            switch (estimation.SelectedWebTechnology)
            {
                case (int)EnumData.WebTechnology.React:
                    webEstimatedAmount = webFixedAmount * 2;
                    break;

                case (int)EnumData.WebTechnology.Angular:
                    webEstimatedAmount = webFixedAmount * 1.5;
                    break;

                case (int)EnumData.WebTechnology.jQuery:
                    webEstimatedAmount = webFixedAmount * 1.5;
                    break;

                case (int)EnumData.WebTechnology.eCommerce:
                    webEstimatedAmount = webFixedAmount * 1.3;
                    break;

                default:
                    webEstimatedAmount = webFixedAmount * 2;
                    break;
            }

            webEstimatedAmount = webEstimatedAmount + (estimation.AmountOfScreenWeb * webPerScreenFixedAmount);
            webEstimatedAmount = webEstimatedAmount + (estimation.DesignsWeb == "on" ? estimation.AmountOfScreenWeb * webPerDesignFixedAmount : 0);
            webEstimatedAmount = webEstimatedAmount + (estimation.DatabaseWeb == "on" ? webDatabaseFixedAmount : 0);
            webEstimatedAmount = webEstimatedAmount + (estimation.APIWeb == "on" ? webAPIFixedAmount : 0);
            webEstimatedAmount = webEstimatedAmount + (estimation.SecurityLayerWeb == "on" ? webSecurityLayerFixedAmount : 0);
            webEstimatedAmount = webEstimatedAmount + (estimation.CloudServersWeb == "on" ? webCloudServerFixedAmount : 0);
            webEstimatedAmount = webEstimatedAmount + (estimation.ExternalApiWeb == "on" ? webExternalApiFixedAmount : 0);
            webEstimatedAmount = webEstimatedAmount + (estimation.SpecialSdkWeb == "on" ? webSpecialSdkFixedAmount : 0);

            return webEstimatedAmount;
        }

        /// <summary>
        /// Calculates the mobile estimation.
        /// </summary>
        /// <param name="estimation">The estimation.</param>
        /// <returns></returns>
        private double CalculateMobileEstimation(Estimation estimation)
        {
            double mobileEstimatedAmount = 0;
            List<string> selectedTechnologyList = new List<string>();
            mobileEstimatedAmount = mobileEstimatedAmount + (estimation.AmountOfScreenMobile * mobilePerScreenFixedAmount);
            mobileEstimatedAmount = mobileEstimatedAmount + (estimation.DesignsMobile == "on" ? estimation.AmountOfScreenMobile * mobilePerDesignFixedAmount : 0);
            mobileEstimatedAmount = mobileEstimatedAmount + (estimation.DatabaseMobile == "on" ? mobileDatabaseFixedAmount : 0);
            mobileEstimatedAmount = mobileEstimatedAmount + (estimation.APIMobile == "on" ? mobileAPIFixedAmount : 0);
            mobileEstimatedAmount = mobileEstimatedAmount + (estimation.SecurityLayerMobile == "on" ? mobileSecurityLayerFixedAmount : 0);
            mobileEstimatedAmount = mobileEstimatedAmount + (estimation.CloudServersMobile == "on" ? mobileCloudServerFixedAmount : 0);
            mobileEstimatedAmount = mobileEstimatedAmount + (estimation.ExternalApiMobile == "on" ? mobileExternalApiFixedAmount : 0);
            mobileEstimatedAmount = mobileEstimatedAmount + (estimation.SpecialSdkMobile == "on" ? mobileSpecialSdkFixedAmount : 0);

            if (estimation.SelectedMobileTechnology == (int)EnumData.MobileTechnology.Both)
            {
                mobileEstimatedAmount = mobileEstimatedAmount * 1.8;
            }
            return mobileEstimatedAmount;
        }

        /// <summary>
        /// Sets the client email parameters.
        /// </summary>
        /// <param name="estimation">The estimation.</param>
        /// <param name="emailRequest">The email request.</param>
        /// <returns></returns>
        private EmailRequest SetClientEmailParams(Estimation estimation, EmailRequest emailRequest, double estimatedAmount, string fileName)
        {
            if (Utils.ConfigKey("SendEmailVia") == "sendgrid")
            {
                emailRequest.FromEmail = Utils.ConfigKey("FromEmail");
                emailRequest.ToEmail = estimation.ClientEmail;
                emailRequest.Subject = "Project Estimation Details";
                emailRequest.DisplayName = Utils.ConfigKey("DisplayName");
            }
            else
            {
                emailRequest.FromEmail = Utils.ConfigKey("GoogleFromEmail");
                emailRequest.Password = Utils.ConfigKey("GoogleEmailPwd");
                emailRequest.Port = Convert.ToInt32(Utils.ConfigKey("Port"));
                emailRequest.Host = Utils.ConfigKey("Host");
                emailRequest.EnableSsl = Utils.ConfigKey("EnableSsl").ToSafeBool(false);
                emailRequest.ToEmail = estimation.ClientEmail;
                emailRequest.Subject = "Project Estimation Details";
                emailRequest.DisplayName = Utils.ConfigKey("DisplayName");
            }
            
            string emailTemplate = string.Empty;
            if (estimation.CrawlingPOCText != null || estimation.MobilePOCText != null)
            {
                emailTemplate = System.IO.File.ReadAllText(Server.MapPath("~/EmailTemplate/MobileCrawling_ltr.html"));
                switch (estimation.SelectedPlatform)
                {
                    case (int)EnumData.Platform.MobilePOC:
                        emailTemplate = emailTemplate.Replace("##PLATFORM##", StringEnum.GetStringValue(EnumData.Platform.MobilePOC));
                        emailTemplate = emailTemplate.Replace("##TEXT##", estimation.MobilePOCText.ToSafeString() != string.Empty ? estimation.MobilePOCText : "-");
                        break;
                    case (int)EnumData.Platform.Crawling:
                        emailTemplate = emailTemplate.Replace("##PLATFORM##", string.Format("{0}", StringEnum.GetStringValue(EnumData.Platform.Crawling)));
                        emailTemplate = emailTemplate.Replace("##TEXT##", estimation.CrawlingPOCText.ToSafeString() != string.Empty ? estimation.CrawlingPOCText : "-");
                        break;
                    default:
                        emailTemplate = emailTemplate.Replace("##PLATFORM##", StringEnum.GetStringValue(EnumData.Platform.MobilePOC));
                        break;
                }
                emailRequest.Body = emailTemplate;
            }
            else
            {
                emailTemplate = System.IO.File.ReadAllText(Server.MapPath("~/EmailTemplate/Estimation_ltr.html"));
                List<string> selectedTechnologies = new List<string>();

                emailTemplate = emailTemplate.Replace("##ESTIMATEDAMOUNT##", string.Format("{0} {1}", estimatedAmount.ToString("N0"), currency));

                switch (estimation.ProjectCategory)
                {
                    case (int)EnumData.ProjectCategory.Travel:
                        emailTemplate = emailTemplate.Replace("##PROJECTCATEGORY##", StringEnum.GetStringValue(EnumData.ProjectCategory.Travel));
                        break;

                    case (int)EnumData.ProjectCategory.Digital:
                        emailTemplate = emailTemplate.Replace("##PROJECTCATEGORY##", StringEnum.GetStringValue(EnumData.ProjectCategory.Digital));
                        break;

                    case (int)EnumData.ProjectCategory.DataSecurity:
                        emailTemplate = emailTemplate.Replace("##PROJECTCATEGORY##", StringEnum.GetStringValue(EnumData.ProjectCategory.DataSecurity));
                        break;

                    case (int)EnumData.ProjectCategory.Productivity:
                        emailTemplate = emailTemplate.Replace("##PROJECTCATEGORY##", StringEnum.GetStringValue(EnumData.ProjectCategory.Productivity));
                        break;

                    case (int)EnumData.ProjectCategory.Gaming:
                        emailTemplate = emailTemplate.Replace("##PROJECTCATEGORY##", StringEnum.GetStringValue(EnumData.ProjectCategory.Gaming));
                        break;

                    case (int)EnumData.ProjectCategory.Business:
                        emailTemplate = emailTemplate.Replace("##PROJECTCATEGORY##", StringEnum.GetStringValue(EnumData.ProjectCategory.Business));
                        break;

                    case (int)EnumData.ProjectCategory.Educational:
                        emailTemplate = emailTemplate.Replace("##PROJECTCATEGORY##", StringEnum.GetStringValue(EnumData.ProjectCategory.Educational));
                        break;

                    case (int)EnumData.ProjectCategory.Lifestyle:
                        emailTemplate = emailTemplate.Replace("##PROJECTCATEGORY##", StringEnum.GetStringValue(EnumData.ProjectCategory.Lifestyle));
                        break;

                    case (int)EnumData.ProjectCategory.Entertainment:
                        emailTemplate = emailTemplate.Replace("##PROJECTCATEGORY##", StringEnum.GetStringValue(EnumData.ProjectCategory.Entertainment));
                        break;

                    case (int)EnumData.ProjectCategory.Utility:
                        emailTemplate = emailTemplate.Replace("##PROJECTCATEGORY##", StringEnum.GetStringValue(EnumData.ProjectCategory.Utility));
                        break;

                    case (int)EnumData.ProjectCategory.Other:
                        emailTemplate = emailTemplate.Replace("##PROJECTCATEGORY##", StringEnum.GetStringValue(EnumData.ProjectCategory.Other));
                        break;

                    default:
                        emailTemplate = emailTemplate.Replace("##PROJECTCATEGORY##", StringEnum.GetStringValue(EnumData.ProjectCategory.Other));
                        break;
                }

                switch (estimation.SelectedPlatform)
                {
                    case (int)EnumData.Platform.Web:
                        emailTemplate = emailTemplate.Replace("##PLATFORM##", StringEnum.GetStringValue(EnumData.Platform.Web));
                        break;

                    case (int)EnumData.Platform.Mobile:
                        emailTemplate = emailTemplate.Replace("##PLATFORM##", StringEnum.GetStringValue(EnumData.Platform.Mobile));
                        break;

                    case (int)EnumData.Platform.WebMobile:
                        emailTemplate = emailTemplate.Replace("##PLATFORM##", string.Format("{0}", StringEnum.GetStringValue(EnumData.Platform.WebMobile)));
                        break;

                    case (int)EnumData.Platform.MobilePOC:
                        emailTemplate = emailTemplate.Replace("##PLATFORM##", StringEnum.GetStringValue(EnumData.Platform.MobilePOC));
                        break;

                    case (int)EnumData.Platform.Crawling:
                        emailTemplate = emailTemplate.Replace("##PLATFORM##", string.Format("{0}", StringEnum.GetStringValue(EnumData.Platform.Crawling)));
                        break;

                    default:
                        emailTemplate = emailTemplate.Replace("##PLATFORM##", StringEnum.GetStringValue(EnumData.Platform.Web));
                        break;
                }

                emailTemplate = emailTemplate.Replace("##TECHNOLOGY##", (estimation.SelectedPlatform == (int)EnumData.Platform.Web
                                                                                || estimation.SelectedPlatform == (int)EnumData.Platform.Mobile
                                                                                || estimation.SelectedPlatform == (int)EnumData.Platform.WebMobile
                                                                                ? GetTechnology(estimation.SelectedPlatform, estimation.SelectedWebTechnology, estimation.SelectedMobileTechnology) : "-"));
                emailTemplate = emailTemplate.Replace("##SCREEN##", (estimation.SelectedPlatform == (int)EnumData.Platform.Web
                                                                                || estimation.SelectedPlatform == (int)EnumData.Platform.Mobile
                                                                                || estimation.SelectedPlatform == (int)EnumData.Platform.WebMobile
                                                                                ? GetScreen(estimation.SelectedPlatform, estimation.AmountOfScreenWeb, estimation.AmountOfScreenMobile) : "-"));

                if (estimation.SelectedPlatform == (int)EnumData.Platform.Web || estimation.SelectedPlatform == (int)EnumData.Platform.WebMobile)
                {
                    string webFunctionality = GetWebFunctionality(estimation);
                    if (webFunctionality.ToSafeString() != string.Empty)
                    {
                        emailTemplate = emailTemplate.Replace("##WEBTECHNOLOGYTR##", string.Empty);
                        emailTemplate = emailTemplate.Replace("##WEBFUNCTIONALITY##", webFunctionality);
                    }
                    else
                    {
                        emailTemplate = emailTemplate.Replace("##WEBTECHNOLOGYTR##", "style='display:none;'");
                    }
                }
                else
                {
                    emailTemplate = emailTemplate.Replace("##WEBTECHNOLOGYTR##", "style='display:none;'");
                }

                if (estimation.SelectedPlatform == (int)EnumData.Platform.Mobile || estimation.SelectedPlatform == (int)EnumData.Platform.WebMobile)
                {
                    string mobileFunctionality = GetMobileFunctionality(estimation);
                    if (mobileFunctionality.ToSafeString() != string.Empty)
                    {
                        emailTemplate = emailTemplate.Replace("##MOBILETECHNOLOGYTR##", string.Empty);
                        emailTemplate = emailTemplate.Replace("##MOBILEFUNCTIONALITY##", mobileFunctionality);
                    }
                    else
                    {
                        emailTemplate = emailTemplate.Replace("##MOBILETECHNOLOGYTR##", "style='display:none;'");
                    }
                }
                else
                {
                    emailTemplate = emailTemplate.Replace("##MOBILETECHNOLOGYTR##", "style='display:none;'");
                }
                if (estimation.Urgency == "on")
                {
                    emailTemplate = emailTemplate.Replace("##URGENCY##", estimation.Urgency);

                }
                else
                {
                    emailTemplate = emailTemplate.Replace("##URGENCYTR##", "style='display:none;'");
                }
                emailTemplate = emailTemplate.Replace("##DEVELOPER##", estimation.Developer);

                emailRequest.Body = emailTemplate;
            }

            if (!Directory.Exists(Server.MapPath(Utils.ConfigKey("ClientEmailLog"))))
            {
                Directory.CreateDirectory(Server.MapPath(Utils.ConfigKey("ClientEmailLog")));
            }

            System.IO.File.WriteAllText(Server.MapPath(string.Format("{0}{1}.html", Utils.ConfigKey("ClientEmailLog"), fileName)), emailTemplate);

            return emailRequest;
        }

        /// <summary>
        /// Sets the admin email parameters.
        /// </summary>
        /// <param name="estimation">The estimation.</param>
        /// <param name="emailRequest">The email request.</param>
        /// <returns></returns>
        private EmailRequest SetAdminEmailParams(Estimation estimation, EmailRequest emailRequest, double estimatedAmount, string fileName)
        {
            if (Utils.ConfigKey("SendEmailVia") == "sendgrid")
            {
                emailRequest.FromEmail = Utils.ConfigKey("FromEmail");
                emailRequest.ToEmail = Utils.ConfigKey("AdminEmail");
                emailRequest.DisplayName = Utils.ConfigKey("DisplayName");
                emailRequest.Subject = "Project Estimation Details";
            }
            else // google
            {
                emailRequest.FromEmail = Utils.ConfigKey("GoogleFromEmail");
                emailRequest.Password = Utils.ConfigKey("GoogleEmailPwd");
                emailRequest.Port = Convert.ToInt32(Utils.ConfigKey("Port"));
                emailRequest.Host = Utils.ConfigKey("Host");
                emailRequest.EnableSsl = Utils.ConfigKey("EnableSsl").ToSafeBool(false);
                emailRequest.ToEmail = Utils.ConfigKey("AdminEmail");
                emailRequest.Subject = "Project Estimation Details";
                emailRequest.DisplayName = Utils.ConfigKey("DisplayName");
            }
            
            emailRequest.Subject = "New Client Estimation - " + estimation.ClientEmail;


            string emailTemplate = string.Empty;
            if (estimation.CrawlingPOCText != null || estimation.MobilePOCText != null)
            {
                emailTemplate = System.IO.File.ReadAllText(Server.MapPath("~/EmailTemplate/MobileCrawling_Admin_ltr.html"));
                emailTemplate = emailTemplate.Replace("##CLIENTEMAIL##", estimation.ClientEmail.ToSafeString());
                emailTemplate = emailTemplate.Replace("##CLIENTMOBILENO##", estimation.ClientMobile.ToSafeString());
                switch (estimation.SelectedPlatform)
                {
                    case (int)EnumData.Platform.MobilePOC:
                        emailTemplate = emailTemplate.Replace("##PLATFORM##", StringEnum.GetStringValue(EnumData.Platform.MobilePOC));
                        emailTemplate = emailTemplate.Replace("##TEXT##", estimation.MobilePOCText.ToSafeString() != string.Empty ? estimation.MobilePOCText : "-");
                        break;

                    case (int)EnumData.Platform.Crawling:
                        emailTemplate = emailTemplate.Replace("##PLATFORM##", string.Format("{0}", StringEnum.GetStringValue(EnumData.Platform.Crawling)));
                        emailTemplate = emailTemplate.Replace("##TEXT##", estimation.CrawlingPOCText.ToSafeString() != string.Empty ? estimation.CrawlingPOCText : "-");
                        break;
                    default:
                        emailTemplate = emailTemplate.Replace("##PLATFORM##", StringEnum.GetStringValue(EnumData.Platform.MobilePOC));
                        break;
                }
                emailRequest.Body = emailTemplate;
            }
            else
            {
                emailTemplate = System.IO.File.ReadAllText(Server.MapPath("~/EmailTemplate/Estimation_Admin_ltr.html"));
                List<string> selectedTechnologies = new List<string>();
                emailTemplate = emailTemplate.Replace("##CLIENTEMAIL##", estimation.ClientEmail.ToSafeString());
                emailTemplate = emailTemplate.Replace("##CLIENTMOBILENO##", estimation.ClientMobile.ToSafeString());
                emailTemplate = emailTemplate.Replace("##ESTIMATEDAMOUNT##", string.Format("{0} {1}", estimatedAmount.ToString("N0"), currency));

                switch (estimation.ProjectCategory)
                {
                    case (int)EnumData.ProjectCategory.Travel:
                        emailTemplate = emailTemplate.Replace("##PROJECTCATEGORY##", StringEnum.GetStringValue(EnumData.ProjectCategory.Travel));
                        break;

                    case (int)EnumData.ProjectCategory.Digital:
                        emailTemplate = emailTemplate.Replace("##PROJECTCATEGORY##", StringEnum.GetStringValue(EnumData.ProjectCategory.Digital));
                        break;

                    case (int)EnumData.ProjectCategory.DataSecurity:
                        emailTemplate = emailTemplate.Replace("##PROJECTCATEGORY##", StringEnum.GetStringValue(EnumData.ProjectCategory.DataSecurity));
                        break;

                    case (int)EnumData.ProjectCategory.Productivity:
                        emailTemplate = emailTemplate.Replace("##PROJECTCATEGORY##", StringEnum.GetStringValue(EnumData.ProjectCategory.Productivity));
                        break;

                    case (int)EnumData.ProjectCategory.Gaming:
                        emailTemplate = emailTemplate.Replace("##PROJECTCATEGORY##", StringEnum.GetStringValue(EnumData.ProjectCategory.Gaming));
                        break;

                    case (int)EnumData.ProjectCategory.Business:
                        emailTemplate = emailTemplate.Replace("##PROJECTCATEGORY##", StringEnum.GetStringValue(EnumData.ProjectCategory.Business));
                        break;

                    case (int)EnumData.ProjectCategory.Educational:
                        emailTemplate = emailTemplate.Replace("##PROJECTCATEGORY##", StringEnum.GetStringValue(EnumData.ProjectCategory.Educational));
                        break;

                    case (int)EnumData.ProjectCategory.Lifestyle:
                        emailTemplate = emailTemplate.Replace("##PROJECTCATEGORY##", StringEnum.GetStringValue(EnumData.ProjectCategory.Lifestyle));
                        break;

                    case (int)EnumData.ProjectCategory.Entertainment:
                        emailTemplate = emailTemplate.Replace("##PROJECTCATEGORY##", StringEnum.GetStringValue(EnumData.ProjectCategory.Entertainment));
                        break;

                    case (int)EnumData.ProjectCategory.Utility:
                        emailTemplate = emailTemplate.Replace("##PROJECTCATEGORY##", StringEnum.GetStringValue(EnumData.ProjectCategory.Utility));
                        break;

                    case (int)EnumData.ProjectCategory.Other:
                        emailTemplate = emailTemplate.Replace("##PROJECTCATEGORY##", StringEnum.GetStringValue(EnumData.ProjectCategory.Other));
                        break;

                    default:
                        emailTemplate = emailTemplate.Replace("##PROJECTCATEGORY##", StringEnum.GetStringValue(EnumData.ProjectCategory.Other));
                        break;
                }

                switch (estimation.SelectedPlatform)
                {
                    case (int)EnumData.Platform.Web:
                        emailTemplate = emailTemplate.Replace("##PLATFORM##", StringEnum.GetStringValue(EnumData.Platform.Web));
                        break;

                    case (int)EnumData.Platform.Mobile:
                        emailTemplate = emailTemplate.Replace("##PLATFORM##", StringEnum.GetStringValue(EnumData.Platform.Mobile));
                        break;

                    case (int)EnumData.Platform.WebMobile:
                        emailTemplate = emailTemplate.Replace("##PLATFORM##", string.Format("{0}", StringEnum.GetStringValue(EnumData.Platform.WebMobile)));
                        break;

                    case (int)EnumData.Platform.MobilePOC:
                        emailTemplate = emailTemplate.Replace("##PLATFORM##", StringEnum.GetStringValue(EnumData.Platform.MobilePOC));
                        break;

                    case (int)EnumData.Platform.Crawling:
                        emailTemplate = emailTemplate.Replace("##PLATFORM##", string.Format("{0}", StringEnum.GetStringValue(EnumData.Platform.Crawling)));
                        break;

                    default:
                        emailTemplate = emailTemplate.Replace("##PLATFORM##", StringEnum.GetStringValue(EnumData.Platform.Web));
                        break;
                }

                emailTemplate = emailTemplate.Replace("##TECHNOLOGY##", (estimation.SelectedPlatform == (int)EnumData.Platform.Web
                                                                                 || estimation.SelectedPlatform == (int)EnumData.Platform.Mobile
                                                                                 || estimation.SelectedPlatform == (int)EnumData.Platform.WebMobile
                                                                                ? GetTechnology(estimation.SelectedPlatform, estimation.SelectedWebTechnology, estimation.SelectedMobileTechnology) : "-"));
                emailTemplate = emailTemplate.Replace("##SCREEN##", (estimation.SelectedPlatform == (int)EnumData.Platform.Web
                                                                                 || estimation.SelectedPlatform == (int)EnumData.Platform.Mobile
                                                                                 || estimation.SelectedPlatform == (int)EnumData.Platform.WebMobile
                                                                                ? GetScreen(estimation.SelectedPlatform, estimation.AmountOfScreenWeb, estimation.AmountOfScreenMobile) : "-"));

                if (estimation.SelectedPlatform == (int)EnumData.Platform.Web || estimation.SelectedPlatform == (int)EnumData.Platform.WebMobile)
                {
                    string webFunctionality = GetWebFunctionality(estimation);
                    if (webFunctionality.ToSafeString() != string.Empty)
                    {
                        emailTemplate = emailTemplate.Replace("##WEBTECHNOLOGYTR##", string.Empty);
                        emailTemplate = emailTemplate.Replace("##WEBFUNCTIONALITY##", webFunctionality);
                    }
                    else
                    {
                        emailTemplate = emailTemplate.Replace("##WEBTECHNOLOGYTR##", "style='display:none;'");
                    }
                }
                else
                {
                    emailTemplate = emailTemplate.Replace("##WEBTECHNOLOGYTR##", "style='display:none;'");
                }
                if (estimation.SelectedPlatform == (int)EnumData.Platform.Mobile || estimation.SelectedPlatform == (int)EnumData.Platform.WebMobile)
                {
                    string mobileFunctionality = GetMobileFunctionality(estimation);
                    if (mobileFunctionality.ToSafeString() != string.Empty)
                    {
                        emailTemplate = emailTemplate.Replace("##MOBILETECHNOLOGYTR##", string.Empty);
                        emailTemplate = emailTemplate.Replace("##MOBILEFUNCTIONALITY##", mobileFunctionality);
                    }
                    else
                    {
                        emailTemplate = emailTemplate.Replace("##MOBILETECHNOLOGYTR##", "style='display:none;'");
                    }
                }
                else
                {
                    emailTemplate = emailTemplate.Replace("##MOBILETECHNOLOGYTR##", "style='display:none;'");
                }
                if (estimation.Urgency == "on")
                {
                    emailTemplate = emailTemplate.Replace("##URGENCY##", estimation.Urgency);
                }
                else
                {
                    emailTemplate = emailTemplate.Replace("##URGENCYTR##", "style='display:none;'");
                }
                emailTemplate = emailTemplate.Replace("##DEVELOPER##", estimation.Developer);

                emailRequest.Body = emailTemplate;
            }

            if (!Directory.Exists(Server.MapPath(Utils.ConfigKey("AdminEmailLog"))))
            {
                Directory.CreateDirectory(Server.MapPath(Utils.ConfigKey("AdminEmailLog")));
            }

            System.IO.File.WriteAllText(Server.MapPath(string.Format("{0}{1}.html", Utils.ConfigKey("AdminEmailLog"), fileName)), emailTemplate);

            return emailRequest;
        }

        /// <summary>
        /// Gets the technology.
        /// </summary>
        /// <param name="selectedPlatform">The selected platform.</param>
        /// <param name="selectedWebTechnology">The selected web technology.</param>
        /// <param name="selectedMobileTechnology">The selected mobile technology.</param>
        /// <returns></returns>
        private string GetTechnology(int selectedPlatform, int selectedWebTechnology, int selectedMobileTechnology)
        {
            List<string> selectedTechnologyList = new List<string>();
            switch (selectedPlatform)
            {
                case (int)EnumData.Platform.Web:
                    switch (selectedWebTechnology)
                    {
                        case (int)EnumData.WebTechnology.React:
                            selectedTechnologyList.Add(EnumData.WebTechnology.React.ToString());
                            break;

                        case (int)EnumData.WebTechnology.Angular:
                            selectedTechnologyList.Add(EnumData.WebTechnology.Angular.ToString());
                            break;

                        case (int)EnumData.WebTechnology.jQuery:
                            selectedTechnologyList.Add(EnumData.WebTechnology.jQuery.ToString());
                            break;

                        case (int)EnumData.WebTechnology.eCommerce:
                            selectedTechnologyList.Add(EnumData.WebTechnology.eCommerce.ToString());
                            break;

                        default:
                            selectedTechnologyList.Add(EnumData.WebTechnology.React.ToString());
                            break;
                    }
                    break;

                case (int)EnumData.Platform.Mobile:
                    switch (selectedMobileTechnology)
                    {
                        case (int)EnumData.MobileTechnology.Android:
                            selectedTechnologyList.Add(EnumData.MobileTechnology.Android.ToString());
                            break;

                        case (int)EnumData.MobileTechnology.iPhone:
                            selectedTechnologyList.Add(EnumData.MobileTechnology.iPhone.ToString());
                            break;

                        case (int)EnumData.MobileTechnology.Both:
                            selectedTechnologyList.Add(EnumData.MobileTechnology.Android.ToString());
                            selectedTechnologyList.Add(EnumData.MobileTechnology.iPhone.ToString());
                            break;

                        default:
                            selectedTechnologyList.Add(EnumData.MobileTechnology.Android.ToString());
                            break;
                    }
                    break;

                case (int)EnumData.Platform.WebMobile:
                    switch (selectedWebTechnology)
                    {
                        case (int)EnumData.WebTechnology.React:
                            selectedTechnologyList.Add(EnumData.WebTechnology.React.ToString());
                            break;

                        case (int)EnumData.WebTechnology.Angular:
                            selectedTechnologyList.Add(EnumData.WebTechnology.Angular.ToString());
                            break;

                        case (int)EnumData.WebTechnology.jQuery:
                            selectedTechnologyList.Add(EnumData.WebTechnology.jQuery.ToString());
                            break;

                        case (int)EnumData.WebTechnology.eCommerce:
                            selectedTechnologyList.Add(EnumData.WebTechnology.eCommerce.ToString());
                            break;

                        default:
                            selectedTechnologyList.Add(EnumData.WebTechnology.React.ToString());
                            break;
                    }

                    switch (selectedMobileTechnology)
                    {
                        case (int)EnumData.MobileTechnology.Android:
                            selectedTechnologyList.Add(EnumData.MobileTechnology.Android.ToString());
                            break;

                        case (int)EnumData.MobileTechnology.iPhone:
                            selectedTechnologyList.Add(EnumData.MobileTechnology.iPhone.ToString());
                            break;

                        case (int)EnumData.MobileTechnology.Both:
                            selectedTechnologyList.Add(EnumData.MobileTechnology.Android.ToString());
                            selectedTechnologyList.Add(EnumData.MobileTechnology.iPhone.ToString());
                            break;

                        default:
                            selectedTechnologyList.Add(EnumData.MobileTechnology.Android.ToString());
                            break;
                    }
                    break;

                case (int)EnumData.Platform.MobilePOC:
                    switch (selectedMobileTechnology)
                    {
                        case (int)EnumData.MobileTechnology.Android:
                            selectedTechnologyList.Add(EnumData.MobileTechnology.Android.ToString());
                            break;

                        case (int)EnumData.MobileTechnology.iPhone:
                            selectedTechnologyList.Add(EnumData.MobileTechnology.iPhone.ToString());
                            break;

                        case (int)EnumData.MobileTechnology.Both:
                            selectedTechnologyList.Add(EnumData.MobileTechnology.Android.ToString());
                            selectedTechnologyList.Add(EnumData.MobileTechnology.iPhone.ToString());
                            break;

                        default:
                            selectedTechnologyList.Add(EnumData.MobileTechnology.Android.ToString());
                            break;
                    }
                    break;

                case (int)EnumData.Platform.Crawling:
                    switch (selectedWebTechnology)
                    {
                        case (int)EnumData.WebTechnology.React:
                            selectedTechnologyList.Add(EnumData.WebTechnology.React.ToString());
                            break;

                        case (int)EnumData.WebTechnology.Angular:
                            selectedTechnologyList.Add(EnumData.WebTechnology.Angular.ToString());
                            break;

                        case (int)EnumData.WebTechnology.jQuery:
                            selectedTechnologyList.Add(EnumData.WebTechnology.jQuery.ToString());
                            break;
                        case (int)EnumData.WebTechnology.eCommerce:
                            selectedTechnologyList.Add(EnumData.WebTechnology.eCommerce.ToString());
                            break;

                        default:
                            selectedTechnologyList.Add(EnumData.WebTechnology.React.ToString());
                            break;
                    }

                    switch (selectedMobileTechnology)
                    {
                        case (int)EnumData.MobileTechnology.Android:
                            selectedTechnologyList.Add(EnumData.MobileTechnology.Android.ToString());
                            break;

                        case (int)EnumData.MobileTechnology.iPhone:
                            selectedTechnologyList.Add(EnumData.MobileTechnology.iPhone.ToString());
                            break;

                        case (int)EnumData.MobileTechnology.Both:
                            selectedTechnologyList.Add(EnumData.MobileTechnology.Android.ToString());
                            selectedTechnologyList.Add(EnumData.MobileTechnology.iPhone.ToString());
                            break;

                        default:
                            selectedTechnologyList.Add(EnumData.MobileTechnology.Android.ToString());
                            break;
                    }
                    break;

                default:
                    break;
            }
            return ListToCommaSeperated(selectedTechnologyList);
        }

        /// <summary>
        /// Gets the screen.
        /// </summary>
        /// <param name="selectedPlatform">The selected platform.</param>
        /// <param name="amountOfScreensWeb">The amount of screens web.</param>
        /// <param name="amountOfScreensMobile">The amount of screens mobile.</param>
        /// <returns></returns>
        private string GetScreen(int selectedPlatform, int amountOfScreensWeb, int amountOfScreensMobile)
        {
            List<string> screenList = new List<string>();
            switch (selectedPlatform)
            {
                case (int)EnumData.Platform.Web:
                    if (amountOfScreensWeb > 0)
                        screenList.Add(string.Format("{0} ({1})", amountOfScreensWeb, StringEnum.GetStringValue(EnumData.Platform.Web)));
                    break;

                case (int)EnumData.Platform.Mobile:
                    if (amountOfScreensMobile > 0)
                        screenList.Add(string.Format("{0} ({1})", amountOfScreensMobile, StringEnum.GetStringValue(EnumData.Platform.Mobile)));
                    break;

                case (int)EnumData.Platform.WebMobile:
                    if (amountOfScreensWeb > 0)
                        screenList.Add(string.Format("{0} ({1})", amountOfScreensWeb, StringEnum.GetStringValue(EnumData.Platform.Web)));
                    if (amountOfScreensMobile > 0)
                        screenList.Add(string.Format("{0} ({1})", amountOfScreensMobile, StringEnum.GetStringValue(EnumData.Platform.Mobile)));
                    break;

                case (int)EnumData.Platform.MobilePOC:
                    if (amountOfScreensMobile > 0)
                        screenList.Add(string.Format("{0} ({1})", amountOfScreensMobile, StringEnum.GetStringValue(EnumData.Platform.MobilePOC)));
                    break;

                case (int)EnumData.Platform.Crawling:
                    if (amountOfScreensWeb > 0)
                        screenList.Add(string.Format("{0} ({1})", amountOfScreensWeb, StringEnum.GetStringValue(EnumData.Platform.Web)));
                    if (amountOfScreensMobile > 0)
                        screenList.Add(string.Format("{0} ({1})", amountOfScreensMobile, StringEnum.GetStringValue(EnumData.Platform.Mobile)));
                    break;

                default:
                    break;
            }
            return ListToCommaSeperated(screenList);
        }

        /// <summary>
        /// Gets the web functionality.
        /// </summary>
        /// <param name="estimation">The estimation.</param>
        /// <returns></returns>
        private string GetWebFunctionality(Estimation estimation)
        {
            List<string> selectedWebFunctionalityList = new List<string>();
            if (estimation.DesignsWeb == "on")
                selectedWebFunctionalityList.Add("Designs");
            if (estimation.DatabaseWeb == "on")
                selectedWebFunctionalityList.Add("Database");
            if (estimation.APIWeb == "on")
                selectedWebFunctionalityList.Add("API");
            if (estimation.SecurityLayerWeb == "on")
                selectedWebFunctionalityList.Add("Security Layer");
            if (estimation.CloudServersWeb == "on")
                selectedWebFunctionalityList.Add("Cloud Servers");
            if (estimation.ExternalApiWeb == "on")
                selectedWebFunctionalityList.Add("External APIs");
            if (estimation.SpecialSdkWeb == "on")
                selectedWebFunctionalityList.Add("Special SDKs");
            return ListToCommaSeperated(selectedWebFunctionalityList);
        }

        /// <summary>
        /// Gets the mobile functionality.
        /// </summary>
        /// <param name="estimation">The estimation.</param>
        /// <returns></returns>
        private string GetMobileFunctionality(Estimation estimation)
        {
            List<string> selectedMobileFunctionalityList = new List<string>();
            if (estimation.DesignsMobile == "on")
                selectedMobileFunctionalityList.Add("Designs");
            if (estimation.DatabaseMobile == "on")
                selectedMobileFunctionalityList.Add("Database");
            if (estimation.APIMobile == "on")
                selectedMobileFunctionalityList.Add("API");
            if (estimation.SecurityLayerMobile == "on")
                selectedMobileFunctionalityList.Add("Security Layer");
            if (estimation.CloudServersMobile == "on")
                selectedMobileFunctionalityList.Add("Cloud Servers");
            if (estimation.ExternalApiMobile == "on")
                selectedMobileFunctionalityList.Add("External APIs");
            if (estimation.SpecialSdkMobile == "on")
                selectedMobileFunctionalityList.Add("Special SDKs");
            return ListToCommaSeperated(selectedMobileFunctionalityList);
        }

        /// <summary>
        /// Lists to comma seperated.
        /// </summary>
        /// <param name="list">The list.</param>
        /// <returns></returns>
        private string ListToCommaSeperated(List<string> list)
        {
            string commaSeperatedString = string.Empty;

            if (list != null && list.Count > 0)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    if (i == 0)
                        commaSeperatedString = list[i];
                    else
                        commaSeperatedString = string.Format("{0}, {1}", commaSeperatedString, list[i]);
                }
            }

            return commaSeperatedString;
        }

        /// <summary>
        /// Generates the log.
        /// </summary>
        /// <param name="clientEmail">The client email.</param>
        /// <param name="clientMobile">The client mobile number.</param>
        /// <param name="estimatedAmount">The estimated amount.</param>
        /// <param name="htmlFileName">Name of the HTML file.</param>
        private void GenerateLog(string clientEmail, string clientMobile, double estimatedAmount, string htmlFileName)
        {
            string directory = Server.MapPath(Utils.ConfigKey("EstimationLog"));
            string todayDate = DateTime.Now.ToString("dd-MM-yyyy");
            string filePath = string.Format("{0}{1}.txt", directory, todayDate);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine("-----------------------------------------------------------------------------");
                writer.WriteLine("Client Email  : " + clientEmail);
                writer.WriteLine("Client Mobile : " + clientMobile);
                writer.WriteLine("Est. Amount   : " + string.Format("{0} {1}", estimatedAmount.ToString("N0"), currency));
                writer.WriteLine("Html Filename : " + htmlFileName + ".html");
                writer.WriteLine();
            }
        }
    }
}