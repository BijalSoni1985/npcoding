﻿using NPCoding.Common;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace NPCoding.Web
{
    public class NPCodingApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// The HTTP client instance
        /// </summary>
        internal static HttpClient httpClientInstance;

        /// <summary>
        /// Applications the start.
        /// </summary>
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            //Uri baseUri = new Uri(Utils.ConfigKey("APIAddress"));
            Uri baseUri = new Uri("https://www.google.com");
            int ConnectionLeaseTimeoutInSeconds = Utils.ConfigKey("ConnectionLeaseTimeoutInSeconds").ToSafeInt();

            httpClientInstance = new HttpClient();
            httpClientInstance.BaseAddress = baseUri;
            httpClientInstance.DefaultRequestHeaders.Clear();
            httpClientInstance.DefaultRequestHeaders.ConnectionClose = false;
            ServicePointManager.FindServicePoint(baseUri).ConnectionLeaseTimeout = ConnectionLeaseTimeoutInSeconds == 0 ? 60 * 1000 : ConnectionLeaseTimeoutInSeconds;
        }
    }
}