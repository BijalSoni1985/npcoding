﻿namespace NPCoding.Entities.Response
{
    /// <summary>
    /// Class ErrorResponse.
    /// </summary>
    public class ErrorResponse
    {
        /// <summary>
        /// Gets or sets the error.
        /// </summary>
        /// <value>The error.</value>
        public string error { get; set; }

        /// <summary>
        /// Gets or sets the error description.
        /// </summary>
        /// <value>The error description.</value>
        public string error_description { get; set; }
    }

}
