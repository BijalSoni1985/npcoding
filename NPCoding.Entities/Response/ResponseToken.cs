﻿using System;
using System.Collections.Generic;

namespace NPCoding.Entities.Response
{
    public class ResponseToken
    {
        public DateTime challenge_ts { get; set; }
        public float score { get; set; }
        public List<string> ErrorCodes { get; set; }
        public bool Success { get; set; }
        public string hostname { get; set; }
    }
}