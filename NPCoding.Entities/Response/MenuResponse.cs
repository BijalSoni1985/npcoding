﻿
namespace NPCoding.Entities.Response
{
    using Domain.Common;
    public class MenuResponse : CommonProperties
    {

        /// <summary>
        /// Gets or sets the screen master identifier.
        /// </summary>
        /// <value>The screen master identifier.</value>
        public long ScreenMasterId { get; set; }
        /// <summary>
        /// Gets or sets the name of the screen alias.
        /// </summary>
        /// <value>The name of the screen alias.</value>
        public string ScreenAliasName { get; set; }
        /// <summary>
        /// Gets or sets the screen name english.
        /// </summary>
        /// <value>The screen name english.</value>
        public string ScreenNameEnglish { get; set; }
        /// <summary>
        /// Gets or sets the screen name arabic.
        /// </summary>
        /// <value>The screen name arabic.</value>
        public string ScreenNameArabic { get; set; }
        /// <summary>
        /// Gets or sets the screen parent identifier.
        /// </summary>
        /// <value>The screen parent identifier.</value>
        public long ScreenParentId { get; set; }
        /// <summary>
        /// Gets or sets the name of the controller.
        /// </summary>
        /// <value>The name of the controller.</value>
        public string ControllerName { get; set; }
        /// <summary>
        /// Gets or sets the name of the action.
        /// </summary>
        /// <value>The name of the action.</value>
        public string ActionName { get; set; }

    }
}
