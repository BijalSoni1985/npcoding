﻿namespace NPCoding.Entities.Domain.Common
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Class Login.
    /// </summary>
    public class Login
    {
        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        [Required(ErrorMessage ="Email is required.")]
        [EmailAddress(ErrorMessage = "Email is not valid.")]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>The password.</value>
        [Required(ErrorMessage = "Password is required.")]
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the forgot password email.
        /// </summary>
        /// <value>The forgot password email.</value>
        [Required(ErrorMessage = "Email is required.")]
        [EmailAddress(ErrorMessage = "Email is not valid.")]
        public string ForgotPasswordEmail { get; set; }
    }
}
