﻿namespace NPCoding.Entities.Domain.Common
{
    /// <summary>
    /// Class ResourceObject.
    /// </summary>
    public class ResourceObject
    {
        /// <summary>
        /// Gets or sets the culture identifier.
        /// </summary>
        /// <value>The culture identifier.</value>
        public byte CultureId { get; set; }
        
        /// <summary>
        /// Gets or sets the default value.
        /// </summary>
        /// <value>The default value.</value>
        public string DefaultValue { get; set; }

        /// <summary>
        /// Gets or sets the name of the form.
        /// </summary>
        /// <value>The name of the form.</value>
        public string FormName { get; set; }

        /// <summary>
        /// Gets or sets the name of the label.
        /// </summary>
        /// <value>The name of the label.</value>
        public string LabelName { get; set; }
    }
}