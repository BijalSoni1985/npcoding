﻿namespace NPCoding.Entities.Domain.Common
{
    /// <summary>
    /// Class ConfigurationLookup.
    /// </summary>
    /// <seealso cref="CommonProperties" />
    public class ConfigurationLookup
    {
        /// <summary>
        /// Gets or sets the configuration lookup identifier.
        /// </summary>
        /// <value>The configuration lookup identifier.</value>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the key field.
        /// </summary>
        /// <value>The key field.</value>
        public string KeyField { get; set; }

        /// <summary>
        /// Gets or sets the value field.
        /// </summary>
        /// <value>The value field.</value>
        public string ValueField { get; set; }
    }
}
