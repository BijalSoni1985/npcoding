﻿namespace NPCoding.Entities.Domain.Common
{
    using System.ComponentModel;

    /// <summary>
    /// Class Error.
    /// </summary>
    public class Error
    {
        /// <summary>
        /// Gets or sets the error code.
        /// </summary>
        /// <value>The error code.</value>
        [DefaultValue(0)]
        public int ErrorCode { get; set; }

        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        /// <value>The error message.</value>
        [DefaultValue("")]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Gets or sets the tech error.
        /// </summary>
        /// <value>The tech error.</value>
        [DefaultValue(" ")]
        public string TechError { get; set; }

    }
}
