﻿namespace NPCoding.Entities.Request
{
    /// <summary>
    /// Class TokenRequest.
    /// </summary>
    public class TokenRequest
    {
        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>The name of the user.</value>
        public string UserName { get; set; }
        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>The password.</value>
        public string Password { get; set; }

        /// <summary>
        /// The grant type
        /// </summary>
        public string grant_type = "password";
    }
}
