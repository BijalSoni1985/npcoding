﻿namespace NPCoding.Entities.Request
{
    public class Estimation
    {
        public int ProjectCategory { get; set; }
        public int SelectedPlatform { get; set; }
        public int SelectedWebTechnology { get; set; }
        public int SelectedMobileTechnology { get; set; }
        public string MobilePOCText { get; set; }
        public string CrawlingPOCText { get; set; }
        public string DesignsWeb { get; set; }
        public string DatabaseWeb { get; set; }
        public string APIWeb { get; set; }
        public string SecurityLayerWeb { get; set; }
        public string CloudServersWeb { get; set; }
        public string ExternalApiWeb { get; set; }
        public string SpecialSdkWeb { get; set; }
        public int AmountOfScreenWeb { get; set; }
        public string DesignsMobile { get; set; }
        public string DatabaseMobile { get; set; }
        public string APIMobile { get; set; }
        public string SecurityLayerMobile { get; set; }
        public string CloudServersMobile { get; set; }
        public string ExternalApiMobile { get; set; }
        public string SpecialSdkMobile { get; set; }
        public int AmountOfScreenMobile { get; set; }
        public string Urgency { get; set; }
        public string Developer { get; set; }
        public string ClientEmail { get; set; }
        public string ClientMobile { get; set; }
    }
}