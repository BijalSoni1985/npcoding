﻿namespace NPCoding.Common
{
    using Core.Common;
    using Data.Infrastructure;
    using Data.Infrastructure.Common;
    using Entities.Domain.Common;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Caching;

    /// <summary>
    /// Class CacheManager.
    /// </summary>
    public class CacheManager
    {
        /// <summary>
        /// Gets the configuration lookup.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>System.String.</returns>
        public static string GetConfigurationLookup(string key)
        {
            try
            {
                return ConfigurationLookupList.Where(x => x.KeyField == key).Select(y => y.ValueField).FirstOrDefault() ?? string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the configuration lookup list.
        /// </summary>
        /// <value>The configuration lookup list.</value>
        private static List<ConfigurationLookup> ConfigurationLookupList
        {
            get
            {
                if (HttpRuntime.Cache.Get(Constants.CONFIGURATIONLOOKUP) != null)
                {
                    return (List<ConfigurationLookup>)HttpRuntime.Cache.Get(Constants.CONFIGURATIONLOOKUP);
                }
                else
                {
                    List<ConfigurationLookup> systemConfig = new List<ConfigurationLookup>();
                    using (IDatabaseFactory factory = new DatabaseFactory())
                    {
                        ICommonRepository commonRepo = new CommonRepository(factory);
                        HttpRuntime.Cache.Remove(Constants.CONFIGURATIONLOOKUP);
                        systemConfig = commonRepo.GetAllConfigurationLookup();
                        HttpRuntime.Cache.Insert(Constants.CONFIGURATIONLOOKUP, systemConfig, null, DateTime.Today.AddYears(1), Cache.NoSlidingExpiration);
                    }
                    return systemConfig;
                }
            }
        }
    }
}