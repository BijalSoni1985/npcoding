﻿namespace NPCoding.Common
{
    /// <summary>
    /// Class Constants.
    /// </summary>
    public class Constants
    {
        /// <summary>
        /// The configurationlookup
        /// </summary>
        public const string CONFIGURATIONLOOKUP = "ConfigurationLookupList";

        /// <summary>
        /// The resourceobjectlist
        /// </summary>
        public const string RESOURCEOBJECTLIST = "ResourceObjectList";

        /// <summary>
        /// The actions
        /// </summary>
        public const string ACTIONS = "Actions";

        /// <summary>
        /// The screens
        /// </summary>
        public const string SCREENS = "Screens";

        /// <summary>
        /// The usermenu
        /// </summary>
        public const string USERMENU = "UserMenu";
    }
}
