﻿namespace NPCoding.Common
{
    /// <summary>
    /// Class EnumData.
    /// </summary>
    public static class EnumData
    {
        public enum ProjectCategory
        {
            [StringValue("Travel")]
            Travel = 1,
            [StringValue("Digital")]
            Digital = 2,
            [StringValue("Data Security")]
            DataSecurity = 3,
            [StringValue("Productivity")]
            Productivity = 4,
            [StringValue("Gaming")]
            Gaming = 5,
            [StringValue("Business")]
            Business = 6,
            [StringValue("Educational")]
            Educational = 7,
            [StringValue("Lifestyle")]
            Lifestyle = 8,
            [StringValue("Entertainment")]
            Entertainment = 9,
            [StringValue("Utility")]
            Utility = 10,
            [StringValue("Other")]
            Other = -1
        }

        public enum WebTechnology
        {
            React = 1,
            Angular = 2,
            jQuery = 3,
            eCommerce = 4
        }

        public enum MobileTechnology
        {
            Android = 1,
            iPhone = 2,
            Both = 3
        }

        public enum WebMobileTechnology
        {
            WebSection,
            MobileSection
        }

        public enum POCTechnology
        {
            MobilePOC,
            Crawling
        }

        public enum Platform
        {
            [StringValue("Web")]
            Web = 1,
            [StringValue("Mobile")]
            Mobile = 2,
            [StringValue("Web + Mobile")]
            WebMobile = 3,
            [StringValue("Mobile POC")]
            MobilePOC = 4,
            [StringValue("Crawling (Web/Mobile)")]
            Crawling = 5
        }

        public enum Addons
        {
            Designs,
            Database,
            API,
            SecurityLayer,
            CloudServers,
            ExternalAPIs,
            SpecialSDKs,
            AmountOfScreens
        }
    }
}