﻿namespace NPCoding.Common
{
    using Microsoft.VisualBasic;
    using System;
    using System.Configuration;
    using System.Globalization;
    using System.Security.Cryptography;
    using System.Text.RegularExpressions;

    /// <summary>
    /// Class Utils.
    /// </summary>
    public static class Utils
    {
        /// <summary>
        /// To the safe string.
        /// </summary>
        /// <param name="objString">The object string.</param>
        /// <returns>System.String.</returns>
        public static string ToSafeString(this object objString)
        {
            return (objString == null || objString == DBNull.Value) ? string.Empty : objString.ToString().Trim();
        }

        /// <summary>
        /// Nulls the check.
        /// </summary>
        /// <param name="self">The self.</param>
        /// <returns>System.Object.</returns>
        public static object NullCheck(this string self)
        {
            return (string.IsNullOrEmpty(self)) ? (object)DBNull.Value : self;
        }

        /// <summary>
        /// To the safe int.
        /// </summary>
        /// <param name="objString">The object string.</param>
        /// <returns>System.Int32.</returns>
        public static int ToSafeInt(this object objString)
        {
            return (objString == null || objString == DBNull.Value) ? 0 : ToInt(objString.ToString());
        }

        /// <summary>
        /// To the safe int.
        /// </summary>
        /// <param name="objValue">The object value.</param>
        /// <returns>System.Int32.</returns>
        public static int ToSafeInt(this int? objValue)
        {
            return (objValue == null || !objValue.HasValue) ? 0 : objValue.Value;
        }

        /// <summary>
        /// To the safe int.
        /// </summary>
        /// <param name="objString">The object string.</param>
        /// <param name="defValue">The definition value.</param>
        /// <returns>System.Int32.</returns>
        public static int ToSafeInt(this object objString, int defValue)
        {
            return (objString == null || objString == DBNull.Value) ? defValue : ToInt(objString.ToString());
        }

        /// <summary>
        /// To the safe int16.
        /// </summary>
        /// <param name="objValue">The object value.</param>
        /// <returns>System.Int16.</returns>
        public static short ToSafeInt16(this object objValue)
        {
            short result = 0;
            return (objValue == null || objValue == DBNull.Value) ? result : ToInt16(objValue.ToString());
        }

        /// <summary>
        /// To the safe int16.
        /// </summary>
        /// <param name="objString">The object string.</param>
        /// <param name="defValue">The definition value.</param>
        /// <returns>System.Int16.</returns>
        public static short ToSafeInt16(this object objString, short defValue)
        {
            return (objString == null || objString == DBNull.Value) ? defValue : ToInt16(objString.ToString());
        }

        /// <summary>
        /// To the safe double.
        /// </summary>
        /// <param name="objString">The object string.</param>
        /// <returns>System.Double.</returns>
        public static double ToSafeDouble(this object objString)
        {
            return (objString == null || objString == DBNull.Value) ? 0.0D : ToDouble(objString.ToString());
        }

        /// <summary>
        /// To the safe decimal.
        /// </summary>
        /// <param name="objString">The object string.</param>
        /// <returns>System.Decimal.</returns>
        public static decimal ToSafeDecimal(this object objString)
        {
            return (objString == null || objString == DBNull.Value) ? 0 : ToDecimal(objString.ToString());
        }

        /// <summary>
        /// To the safe bool.
        /// </summary>
        /// <param name="objString">The object string.</param>
        /// <param name="defValue">if set to <c>true</c> [definition value].</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool ToSafeBool(this object objString, bool defValue)
        {
            return (objString == null || objString == DBNull.Value) ? defValue : ToBool(objString.ToString());
        }

        /// <summary>
        /// To the safe bool.
        /// </summary>
        /// <param name="objString">if set to <c>true</c> [object string].</param>
        /// <param name="defValue">if set to <c>true</c> [definition value].</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool ToSafeBool(this bool? objString, bool defValue)
        {
            return (objString == null || !objString.HasValue) ? defValue : ToBool(objString.ToString());
        }

        /// <summary>
        /// To the decimal.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns>System.Decimal.</returns>
        public static decimal ToDecimal(string str)
        {
            decimal result = 0;
            if (str != null && Information.IsNumeric(str))
            {
                try
                {
                    result = Convert.ToDecimal(str);
                }
                catch { }
            }
            return result;
        }

        /// <summary>
        /// To the double.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns>System.Double.</returns>
        public static double ToDouble(string str)
        {
            double result = 0;
            if (str != null && Information.IsNumeric(str))
            {
                try
                {
                    result = Convert.ToDouble(str);
                }
                catch
                {
                    return result;
                }
            }
            return result;
        }

        /// <summary>
        /// Converts the string representation of a number to an integer.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns>System.Int32.</returns>
        public static int ToInt(string str)
        {
            int result = 0;
            if (str != null && str.Length > 0)
            {
                try
                {
                    int.TryParse(str, out result);
                }
                catch { }
            }

            return result;
        }

        /// <summary>
        /// To the int16.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns>System.Int16.</returns>
        public static short ToInt16(object str)
        {
            short result = 0;
            if (str != null)
            {
                try
                {
                    short.TryParse(str.ToString(), out result);
                }
                catch { }
            }

            return result;
        }

        /// <summary>
        /// Configurations the key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>System.String.</returns>
        public static string ConfigKey(string key)
        {
            return ConfigurationManager.AppSettings[key].ToSafeString();
        }

        /// <summary>
        /// To the safe long.
        /// </summary>
        /// <param name="objString">The object string.</param>
        /// <returns>System.Int64.</returns>
        public static long ToSafeLong(this object objString)
        {
            return (objString == null || objString == DBNull.Value) ? 0 : ToLong(objString.ToString());
        }

        /// <summary>
        /// To the long.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns>System.Int64.</returns>
        public static long ToLong(string str)
        {
            long result = 0;
            if (str != null && Information.IsNumeric(str))
            {
                try
                {
                    result = Convert.ToInt64(str);
                }
                catch { }
            }
            return result;
        }

        /// <summary>
        /// To the bool.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool ToBool(object data)
        {
            if (data == null)
                return false;
            else
                return ToBool(data.ToString());
        }

        /// <summary>
        /// To the bool.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool ToBool(string data)
        {
            bool result = false;
            if (data != null)
            {
                string tdata = data.Trim().ToLower();
                if (tdata == "true" || tdata == "yes" || tdata == "1")
                    result = true;
                else
                    result = false;
            }
            return result;
        }

        /// <summary>
        /// Get8s the digits.
        /// </summary>
        /// <returns>System.String.</returns>
        public static string Get8Digits()
        {
            var bytes = new byte[4];
            var rng = RandomNumberGenerator.Create();
            rng.GetBytes(bytes);
            uint random = BitConverter.ToUInt32(bytes, 0) % 100000000;
            return String.Format("{0:D8}", random);
        }

        /// <summary>
        /// Checks the null.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj">The object.</param>
        /// <returns>System.Object.</returns>
        public static object CheckNull<T>(object obj)
        {
            object resultValue = DBNull.Value;
            try
            {
                if (obj != null)
                {
                    if (typeof(T) == typeof(DateTime) && Convert.ToDateTime(obj) == DateTime.MinValue)
                    {
                        resultValue = DBNull.Value;
                    }
                    else
                    {
                        resultValue = (T)Convert.ChangeType(obj, typeof(T));
                    }
                }
            }
            catch (Exception)
            {
                return DBNull.Value;
            }
            return resultValue;
        }

        /// <summary>
        /// Gets the random digits.
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <returns>System.String.</returns>
        public static string GetRandomDigits(string prefix)
        {
            var bytes = new byte[4];
            var rng = RandomNumberGenerator.Create();
            rng.GetBytes(bytes);
            uint random = BitConverter.ToUInt32(bytes, 0) % 100000000;
            return prefix + string.Format("{0:D8}", random);
        }

        public static bool IsValidEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return false;

            try
            {
                // Normalize the domain
                email = Regex.Replace(email, @"(@)(.+)$", DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));

                // Examines the domain part of the email and normalizes it.
                string DomainMapper(Match match)
                {
                    // Use IdnMapping class to convert Unicode domain names.
                    var idn = new IdnMapping();

                    // Pull out and process domain name (throws ArgumentException on invalid)
                    var domainName = idn.GetAscii(match.Groups[2].Value);

                    return match.Groups[1].Value + domainName;
                }
            }
            catch (RegexMatchTimeoutException e)
            {
                return false;
            }
            catch (ArgumentException e)
            {
                return false;
            }

            try
            {
                return Regex.IsMatch(email,
                    @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                    @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                    RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }
    }
}