﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SendGrid;
using SendGrid.Helpers.Mail;


namespace NPCoding.Email
{
    public class EmailSendGrid : ISendEmail
    {
        private string APIKey;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailSendGrid"/> class.
        /// </summary>
        public EmailSendGrid()
        {
            APIKey = "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";
        }

        /// <summary>
        /// Sends the email.
        /// </summary>
        /// <param name="emailRequest"></param>
        /// <param name="subject">The subject.</param>
        /// <param name="body">The body.</param>
        /// <returns></returns>
        public async Task<Tuple<string, bool>> SendEmail(EmailRequest emailRequest)
        {
            return await Send(emailRequest);
        }

        /// <summary>
        /// Sends the email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="body">The body.</param>
        /// <returns></returns>
        private async Task<Tuple<string, bool>> Send(EmailRequest emailRequest)
        {
            bool result = false;
            string message = string.Empty;

            try
            {
                SendGridClientOptions sendGridClientOptions = new SendGridClientOptions { ApiKey = APIKey};
                SendGridClient sendGridClient = new SendGridClient(sendGridClientOptions);
                var from = new EmailAddress(emailRequest.FromEmail);
                var subject = emailRequest.Subject;
                var to = new EmailAddress(emailRequest.ToEmail);
                var htmlContent = emailRequest.Body;
                var msg = MailHelper.CreateSingleEmail(from, to, subject, string.Empty, htmlContent);
                //var response = await sendGridClient.SendEmailAsync(msg);
                Response response = await sendGridClient.SendEmailAsync(msg);
                result = true;
                message = "Email sent successfully!";
            }
            catch(Exception ex)
            {
                result = false;
                message = ex.Message;
            }

            return new Tuple<string, bool>(message, result);
        }
    }
}