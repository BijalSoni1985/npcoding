﻿using System;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace NPCoding.Email
{
    public class EmailSMTP : ISendEmail
    {
        public EmailSMTP()
        {
        }

        /// <summary>
        /// Sends the email.
        /// </summary>
        /// <param name="toEmail">To email.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="body">The body.</param>
        /// <returns></returns>
        public async Task<Tuple<string, bool>> SendEmail(EmailRequest emailRequest)
        {
            return await Send(emailRequest);
        }

        /// <summary>
        /// Sends the specified to email.
        /// </summary>
        /// <param name="toEmail">To email.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="body">The body.</param>
        /// <returns></returns>
        private async Task<Tuple<string, bool>> Send(EmailRequest emailRequest)
        {
            bool result = false;
            string message = string.Empty;

            try
            {
                MailMessage mail = new MailMessage();
                mail.To.Add(emailRequest.ToEmail);
                mail.From = new MailAddress(emailRequest.FromEmail, emailRequest.DisplayName);
                mail.Subject = emailRequest.Subject;
                mail.Body = emailRequest.Body;
                mail.IsBodyHtml = true;
                mail.BodyEncoding = Encoding.UTF8;

                if (!string.IsNullOrWhiteSpace(emailRequest.FileName))
                {
                    mail.Attachments.Add(new Attachment(emailRequest.FileName));
                }

                //mail.Attachments.Add(stream);
                mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                SmtpClient client = new SmtpClient();
                client.UseDefaultCredentials = false;
                client.Port = emailRequest.Port;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Credentials = new NetworkCredential(emailRequest.FromEmail, emailRequest.Password);
                client.Host = emailRequest.Host;
                client.EnableSsl = emailRequest.EnableSsl;

                await client.SendMailAsync(mail);

                result = true;
                message = "Email sent successfully!";
            }
            catch (Exception ex)
            {
                result = false;
                message = ex.Message;
            }

            return new Tuple<string, bool>(message, result);
        }
    }
}