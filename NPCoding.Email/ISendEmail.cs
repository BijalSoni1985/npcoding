﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NPCoding.Email
{
    public interface ISendEmail
    {
        /// <summary>
        /// Sends the email.
        /// </summary>
        /// <param name="toEmail">To email.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="body">The body.</param>
        /// <returns></returns>
        Task<Tuple<string, bool>> SendEmail(EmailRequest emailRequest);
    }
}