﻿namespace NPCoding.API
{
    using Core;
    using Core.Common;
    using Data.Infrastructure;
    using Data.Infrastructure.Common;
    using Data.Repository;
    using SimpleInjector;
    using SimpleInjector.Integration.WebApi;
    using SimpleInjector.Lifestyles;
    using System;
    using System.Web;
    using System.Web.Http;
    using System.Web.Mvc;
    using System.Web.Routing;

    public class Global : HttpApplication
    {
        private void Application_Start(object sender, EventArgs e)
        {
            var container = new Container();

            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();

            container.Register<IDatabaseFactory, DatabaseFactory>(Lifestyle.Scoped);
            container.Register<IAuthenticationRepository, AuthenticationRepository>(Lifestyle.Scoped);
            container.Register<ICommonRepository, CommonRepository>(Lifestyle.Scoped);
            container.Verify();

            GlobalConfiguration.Configuration.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}