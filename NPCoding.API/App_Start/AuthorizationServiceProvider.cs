﻿namespace NPCoding.API
{
    using Common;
    using Core;
    using Data.Infrastructure;
    using Data.Repository;
    using Microsoft.Owin.Security.OAuth;
    using System.Security.Claims;
    using System.Threading.Tasks;

    /// <summary>
    /// Class AuthorizationServerProvider.
    /// </summary>
    /// <seealso cref="Microsoft.Owin.Security.OAuth.OAuthAuthorizationServerProvider" />
    /// <seealso cref="OAuthAuthorizationServerProvider" />
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        /// <summary>
        /// Called to validate that the origin of the request is a registered "client_id", and that the correct credentials for that client are
        /// present on the request. If the web application accepts Basic authentication credentials,
        /// context.TryGetBasicCredentials(out clientId, out clientSecret) may be called to acquire those values if present in the request header. If the web
        /// application accepts "client_id" and "client_secret" as form encoded POST parameters,
        /// context.TryGetFormCredentials(out clientId, out clientSecret) may be called to acquire those values if present in the request body.
        /// If context.Validated is not called the request will not proceed further.
        /// </summary>
        /// <param name="context">The context of the event carries information in and results out.</param>
        /// <returns>Task to enable asynchronous execution</returns>
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated(); //
        }

        /// <summary>
        /// Called when a request to the Token endpoint arrives with a "grant_type" of "password". This occurs when the user has provided name and password
        /// credentials directly into the client application's user interface, and the client application is using those to acquire an "access_token" and
        /// optional "refresh_token". If the web application supports the
        /// resource owner credentials grant type it must validate the context.Username and context.Password as appropriate. To issue an
        /// access token the context.Validated must be called with a new ticket containing the claims about the resource owner which should be associated
        /// with the access token. The application should take appropriate measures to ensure that the endpoint isn’t abused by malicious callers.
        /// The default behavior is to reject this grant type.
        /// See also http://tools.ietf.org/html/rfc6749#section-4.3.2
        /// </summary>
        /// <param name="context">The context of the event carries information in and results out.</param>
        /// <returns>Task to enable asynchronous execution</returns>
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            //UserList user = null;
            using (IDatabaseFactory factory = new DatabaseFactory())
            {
                IAuthenticationRepository repository = new AuthenticationRepository(factory);
                //user = repository.AuthenticateUserByParam(context.UserName, SHA1helper.GetSHA1HashData(context.Password.ToSafeString()));
            }

            return;
            //if (user != null)
            //{
            //    if (user.StatusId == (byte)EnumData.StatusId.Active)
            //    {
            //        identity.AddClaim(new Claim(ClaimTypes.Email, context.UserName));
            //        identity.AddClaim(new Claim(ClaimTypes.Role, user.UserTypeName));
            //        identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.UserID.ToSafeString()));
            //        context.Validated(identity);
            //    }
            //    else if (user.StatusId == (byte)EnumData.StatusId.Inactive)
            //    {
            //        context.SetError("invalid_grant", "User is inactive");
            //        return;
            //    }
            //    else if (user.StatusId == (byte)EnumData.StatusId.Suspended)
            //    {
            //        context.SetError("invalid_grant", "User is suspended");
            //        return;
            //    }
            //    else
            //    {
            //        context.SetError("invalid_grant", "Internal Error");
            //        return;
            //    }
            //}
            //else
            //{
            //    context.SetError("invalid_grant", "Provided username and password is incorrect");
            //    return;
            //}
        }
    }
}