﻿namespace NPCoding.API
{
    using Newtonsoft.Json;
    using System.Net;
    using System.Net.Http;
    using System.Web;
    /// <summary>
    /// Class AuthorizeAttribute.
    /// </summary>
    /// <seealso cref="System.Web.Http.AuthorizeAttribute" />
    public class AuthorizeAttribute : System.Web.Http.AuthorizeAttribute
    {
        /// <summary>
        /// Processes requests that fail authorization.
        /// </summary>
        /// <param name="actionContext">The context.</param>
        protected override void HandleUnauthorizedRequest(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                base.HandleUnauthorizedRequest(actionContext);
            }
            else
            {
                actionContext.Response = new HttpResponseMessage(HttpStatusCode.Forbidden) { Content = new StringContent(JsonConvert.SerializeObject(new { Message = "Authorization failed."})), StatusCode = HttpStatusCode.Forbidden };
            }
           
        }
    }
}