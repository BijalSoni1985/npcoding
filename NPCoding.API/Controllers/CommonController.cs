﻿namespace NPCoding.API.Controllers
{
    using Core.Common;
    using System.Web.Http;

    /// <summary>
    /// Class CommonController.
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    [AllowAnonymous]
    public class CommonController : ApiController
    {
        /// <summary>
        /// The common repository
        /// </summary>
        private readonly ICommonRepository _CommonRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommonController"/> class.
        /// </summary>
        /// <param name="commonRepository">The common repository.</param>
        public CommonController(ICommonRepository commonRepository)
        {
            _CommonRepository = commonRepository;
        }
    }
}